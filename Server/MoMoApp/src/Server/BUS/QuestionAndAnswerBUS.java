/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.BUS;

import Server.DAO.AnswerDAO;
import Server.DAO.QuestionDAO;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import utils.DTO.AnswerDTO;
import utils.DTO.QuestionDTO;
import utils.Env.Const;

/**
 *
 * @author nguyen huu duc
 */
public class QuestionAndAnswerBUS {

    private List<QuestionDTO> questions = new ArrayList<>();
    private List<AnswerDTO> answers = new ArrayList<>();
    private LinkedHashMap<QuestionDTO, List<AnswerDTO>> setOfQuestions = new LinkedHashMap<>();

    private QuestionDAO questionDAO = new QuestionDAO("tblquestion", QuestionDTO.class);
    private AnswerDAO answerDAO = new AnswerDAO("tblanswer", AnswerDTO.class);

    public QuestionAndAnswerBUS() {
        loadData();
    }

    private void loadData() {
        if (questions.isEmpty() && answers.isEmpty()) {
            questions.addAll(questionDAO.getAll().values());
            answers.addAll(answerDAO.getAll().values());
        } else {
            questions.clear();
            answers.clear();
            questions.addAll(questionDAO.getAll().values());
            answers.addAll(answerDAO.getAll().values());
        }
    }

    public void createSetOfQuestions() {
        // đảo thứ tự câu hỏi 
        Collections.shuffle(questions);
        for (int i = 1; i <= Const.QUESTION_NUMBER; i++) {
            setOfQuestions.put(questions.get(i), getAnswersOfQuestion(questions.get(i).getId()));
        }
    }

    public LinkedHashMap<QuestionDTO, List<AnswerDTO>> getSetOfQuestions() {
        return setOfQuestions;
    }
    
    public List<AnswerDTO> getAnswersOfQuestion(String questionId) {
        List<AnswerDTO> aw = new ArrayList<>();
        for (int i = 0; i < answers.size(); i++) {
            if (questionId.equals(answers.get(i).getQuestionId())) {
                aw.add(answers.get(i));
            }
        }
        // đảo thứ tự câu trả lời 
        Collections.shuffle(aw);
        return aw;
    }

    public boolean addQuestion(QuestionDTO question) {
        try {
            questionDAO.insert(question);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean addAnswer(List<AnswerDTO> answers) {
        System.out.println("snsdf" + answers.size());
        try {
            for(int i = 0; i < answers.size(); i++) {
                answerDAO.insert(answers.get(i));
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    
    public boolean deleteAnswer(String questionId) {
        if (questionId == null)
            return false;
        
        boolean check = answerDAO.deleteAnswerByQuestionId(questionId);
        
        return check;
    }
    
    public boolean deleteQuestion(String questionId) {
         if (questionId == null)
            return false;
        
        questionDAO.delete(questionId);
        
        return true;
    }
    
    public boolean saveSetOfQuestion(ConcurrentHashMap<QuestionDTO, List<AnswerDTO>> setOfNewQuestion, List<String> deletedQuesiton) {
        try {
            // save new question and answer
            for(QuestionDTO questionDTO : setOfNewQuestion.keySet()) {
                addQuestion(questionDTO);
                addAnswer(setOfNewQuestion.get(questionDTO));
            }

            // delete question and answer
            for(String questionId : deletedQuesiton) {
                deleteAnswer(questionId);
                deleteQuestion(questionId);
            }

            return true;
        } catch (Exception e) {
        }
        return false;
    }
    public List<QuestionDTO> getQuestionList() {
        return this.questions;
    }
    
    public List<AnswerDTO> getAnswerList(String questionId) {
        List<AnswerDTO> answerList = new ArrayList<>();
        
        for (AnswerDTO answerDTO: answers) {
            if (answerDTO.getQuestionId().equals(questionId)) {
                answerList.add(answerDTO);
            }
        }
        
        return answerList;
    }
    
    public int getNumberQuestion() {
        return this.questions.size();
    }
}

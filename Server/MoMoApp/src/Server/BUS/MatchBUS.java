/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.BUS;

import Server.DAO.MatchDAO;
import utils.DTO.MatchDTO;

/**
 *
 * @author nguyen huu duc
 */
public class MatchBUS {
    private MatchDAO matchDAO = new MatchDAO("tblmatch", MatchDTO.class);
    
    public boolean addItem(MatchDTO match) {
        try {
            matchDAO.insert(match);
            return true;
        } catch (Exception e) {
        }
        return false;
    }
}

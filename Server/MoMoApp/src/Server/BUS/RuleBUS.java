/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.BUS;

import Server.DAO.RuleDAO;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import utils.DTO.RuleDTO;
import utils.Env.Const;

/**
 *
 * @author nguyen huu duc
 */
public class RuleBUS {

    public static enum RuleEnum {

        TIME_ANSWER,
        QUESTION_NUMBER,
        POINT_PLUS,
    }

    public static volatile ConcurrentHashMap<String, RuleDTO> rules;
    private RuleDAO ruleDAO = new RuleDAO("tblrule", RuleDTO.class);

    public RuleBUS() {
        loadData();
    }

    public void loadData() {
        if (rules == null) {
            rules = new ConcurrentHashMap<>();
            rules.putAll(ruleDAO.getAll());
        } else {
            rules.clear();
            rules = new ConcurrentHashMap<>();
            rules.putAll(ruleDAO.getAll());
        }
    }

    public void updateRule() {
        rules.keySet().stream().forEach((key) -> {
            if (rules.get(key).getContent().equals(RuleEnum.QUESTION_NUMBER.toString())) {
                Const.QUESTION_NUMBER = rules.get(key).getValue();
            } else if (rules.get(key).getContent().equals(RuleEnum.POINT_PLUS.toString())) {
                Const.POINT_PLUS = rules.get(key).getValue();
            } else if (rules.get(key).getContent().equals(RuleEnum.TIME_ANSWER.toString())) {
                Const.TIME_ANSWER = rules.get(key).getValue();
            }
        });
    }

    public ConcurrentHashMap<String, RuleDTO> getRules() {
        return rules;
    }
    
    public boolean ConfigureRule(RuleDTO rule) {
        try {
            for (String key : rules.keySet()) {
                 ruleDAO.update(rules.get(key));
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean saveRuleInDB(List<RuleDTO> rules) {
        
        try{
            for(RuleDTO ruleDTO : rules) {
                ruleDAO.update(ruleDTO);
            }
            return true;
        } catch(Exception e){
            return false;
        }
    }
    public List<RuleDTO> getList() {
        return ruleDAO.getList();
    }
    
    public static void main(String[] args) {
        RuleBUS bus = new RuleBUS();
        bus.updateRule();
        System.out.println(Const.TIME_ANSWER);
        System.out.println(Const.QUESTION_NUMBER);
        System.out.println(Const.POINT_PLUS);

    }
}

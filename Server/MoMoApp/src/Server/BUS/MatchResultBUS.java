/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.BUS;

import Server.DAO.MatchResultDAO;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DTO.MatchResultDTO;
import utils.DTO.MemberDTO;
import utils.Models.MatchResultModel;
import utils.Models.StatiscalMatchModel;
import utils.Models.StatiscalWinChainModel;
import utils.Models.StatiscalWinModel;

/**
 *
 * @author nguyen huu duc
 */
public class MatchResultBUS {

    private MatchResultDAO dao = new MatchResultDAO("tblmatchresult", MatchResultDTO.class);

    public MatchResultBUS() {

    }

    public boolean addItem(MatchResultDTO resultDTO) {
        try {
            dao.insert(resultDTO);
            return true;
        } catch (Exception e) {
        }
        return false;
    }

    public List<StatiscalWinChainModel> findUserHaveMaxChainOfWin() {
        int maxChainWinOfUser = 0;
        String firstname = "";
        String lastname = "";

        MemberBUS service = new MemberBUS();
        List<MemberDTO> members = service.getList();
        List<MatchResultDTO> matchResultOfUsers = getList();

        List<StatiscalWinChainModel> tam = new ArrayList<>();
        List<StatiscalWinChainModel> result = new ArrayList<>();
        for (MemberDTO member : members) {

            List<MatchResultModel> matchResultOfEachUsers = new ArrayList<>();

            //find result of each user
            for (MatchResultDTO matchResult : matchResultOfUsers) {
                if (member.getId().equals(matchResult.getMember_id())) {
                    matchResultOfEachUsers.add(matchResult.mappingToModel());
                }
            }

            //count win result of each user
            int chainWinOfUser = findMaxChainOfWin(matchResultOfEachUsers);
            if (chainWinOfUser >= maxChainWinOfUser) {
                firstname = member.getFirstname();
                lastname = member.getLastname();
                maxChainWinOfUser = chainWinOfUser;
                StatiscalWinChainModel model = new StatiscalWinChainModel(firstname, lastname, maxChainWinOfUser);
                tam.add(model);
            }

        }

        for (StatiscalWinChainModel model : tam) {
            if (model.getWinChain() == maxChainWinOfUser) {
                result.add(model);
            }
        }
        return result;
    }

    public List<StatiscalMatchModel> findPlayerWithMaxMatchNumber() {
        return dao.findPlayerWithMaxMatchNumber();
    }

    public List<StatiscalWinModel> findPlayerWithMaxWinNumber() {
        return dao.findPlayerWithMaxWinNumber();
    }

    public int findMaxChainOfWin(List<MatchResultModel> results) {
        Collections.sort(results);
        
        int max = 0;
        int chainWin = 0;
        for (int i = 0; i < results.size(); i++) {
            if (results.get(i).isResult()) {
                chainWin++;
            } else {
                chainWin = 0;
            }
            if (max <= chainWin) {
                max = chainWin;
            }
        }
        return max;
    }

    public List<MatchResultDTO> getList() {
        try {
            return dao.getList();
        } catch (Exception e) {
            return null;
        }
    }

    public static void main(String[] args) {
        List<MatchResultDTO> list = new MatchResultBUS().getList();

        Timestamp t = new Timestamp(System.currentTimeMillis());
        String datetime = String.valueOf(t);
        System.out.println(datetime);
        
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        Date date;
        try {
            date = sdf.parse(datetime);
            System.out.println(date);
        } catch (ParseException ex) {
            Logger.getLogger(MatchResultBUS.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        Date d = new Date();
        Timestamp t2 = new Timestamp(d.getTime());
        System.out.println(t2);
//        MatchResultDTO r = new MatchResultDTO();
//        r.setDatetime(dateTime);
//        System.out.println();

    }
}

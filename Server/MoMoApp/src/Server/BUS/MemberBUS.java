package Server.BUS;

import Server.DAO.MemberDAO;
import com.google.gson.Gson;
import java.util.ArrayList;
import utils.DTO.MemberDTO;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import utils.Models.RankingModel;
import java.util.stream.Collectors;

public class MemberBUS {

    public static ConcurrentHashMap<String, MemberDTO> members;
    private MemberDAO memberDAO = new MemberDAO("tblmember", MemberDTO.class);

    public MemberBUS() {
        loadData();
    }

    public void loadData() {
        if (members == null) {
            members = new ConcurrentHashMap<>();
            members.putAll(memberDAO.getAll());
        } else {
            members.clear();
            members = new ConcurrentHashMap<>();
            members.putAll(memberDAO.getAll());
        }
    }

    public MemberDTO loginCheck(String username, String password) {
        loadData();
        for (String id : members.keySet()) {
            String usernameIn = members.get(id).getUsername();
            String passwordIn = members.get(id).getPassword();
            if (username.equals(usernameIn) && password.equals(passwordIn)) {
                return members.get(id);
            }
        }
        return null;
    }

    public boolean addItem(MemberDTO member) {
        try {
            memberDAO.insert(member);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean updateItem(MemberDTO member) {
        try {
            memberDAO.update(member);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean deleteItem(String id) {
        try {
            memberDAO.delete(id);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public ConcurrentHashMap<String, MemberDTO> getAll() {
        return members;
    }

    public MemberDTO getMemberById(String id) {
        for (String key : members.keySet()) {
            if (key.equalsIgnoreCase(id)) {
                return members.get(key);
            }
        }
        return null;
    }

    public List<RankingModel> getRank() {
        List<MemberDTO> tempt = new ArrayList<MemberDTO>(members.values());
        int[] point = {Integer.MIN_VALUE};
        int[] no = {0};
        int[] rank = {0};
        List<RankingModel> ranks = tempt
                .stream()
                .sorted((pointUser1, pointUser2) -> pointUser2.getAccumulatePoint() - pointUser1.getAccumulatePoint())
                .map((Function<? super MemberDTO, ? extends RankingModel>) p -> {
                    ++no[0];
                    if (point[0] != p.getAccumulatePoint()) {
                        rank[0] = no[0];
                    }
                    return new RankingModel(p.getId(), p.getFullName(), point[0] = p.getAccumulatePoint(), rank[0]);
                })
                .collect(Collectors.toList());
        return ranks;
    }

    public List<MemberDTO> getList() {
        return memberDAO.getList();
    }

    public int getTotalMember() {
        return members.size();
    }
    public void lockUser(MemberDTO memberDTO) {
        if(memberDTO == null) return;
        memberDAO.lockUser(memberDTO);
    }
    public static void main(String[] args) {
        MemberBUS ser = new MemberBUS(); 
        
    }
}

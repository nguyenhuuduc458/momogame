package Server.DAO;

import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DTO.MemberDTO;

public class MemberDAO extends BaseDAO<MemberDTO> {

    public MemberDAO(String tableName, Class<MemberDTO> clazz) {
        super(tableName, clazz);
    }
    
    public void lockUser(MemberDTO memberDTO) {
        DatabaseManager manager = new DatabaseManager();
        try {
            String isActive = memberDTO.isActive()? String.valueOf(1) : String.valueOf(0);
            String query = "UPDATE tblmember SET is_active='" + isActive + "' WHERE id='" + memberDTO.getId() + "'";
            System.out.println(query);
            manager.executeUpdate(query);
        } catch (Exception ex) {
            Logger.getLogger(MemberDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

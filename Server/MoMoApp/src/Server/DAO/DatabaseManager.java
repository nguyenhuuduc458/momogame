/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author nguyen huu duc
 */
public class DatabaseManager {
    private String username = "";
    private String password = " ";
    private String database = "";
    private String host = "";
    private Connection connect = null;
    private Statement statement = null;
    private ResultSet result = null;
    
    public DatabaseManager() {
    	this.host = "localhost";
    	this.username = "root";
    	this.password = "";
    	this.database = "momogame";
    }
    
    public DatabaseManager(String host, String username, String password, String database){
        this.host = host;
        this.username = username;
        this.password = password;
        this.database = database;
    }
    
    public void DriverTest() throws Exception{    
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            throw new Exception("MySQLJDBC driver not found");
        }
    }
    
    public Connection getConnect() throws Exception{
        if(this.connect == null){
            DriverTest();
            String url = "jdbc:mysql://"+this.host+":3306/"+this.database+"?useUnicode=yes&characterEncoding=UTF-8";
            try {
                this.connect = DriverManager.getConnection(url,username,password);
            } catch (SQLException ex) {
                throw new Exception("Không thể kết nối được với cơ sở dữ liệu");
            }
        }
        return this.connect;
    }
    
    public Statement getStatement() throws Exception{
        if(this.statement == null ? true : this.statement.isClosed()){
            this.statement = this.getConnect().createStatement();
        }
      return this.statement;
    }
    
    public ResultSet executeQuery(String query) throws Exception{
        try {
            this.result = this.getStatement().executeQuery(query);
        } catch (Exception ex) {
            throw new Exception("Lỗi: "+ex.getMessage()+ "- Query: " + query);
        }
        return this.result;
    }
    
    public int executeUpdate(String query) throws Exception{
        int res;
        try {
            res = this.getStatement().executeUpdate(query);
        } catch (Exception ex) {
        	throw new Exception("Lỗi: "+ex.getMessage()+ "- Query: " + query);
        }
        return res;
    }
    
    public void close() throws Exception{
        if(this.result != null && !this.result.isClosed()){
            result.close();
            this.result = null;
        }
        if(this.statement != null && !this.statement.isClosed()){
            statement.close();
            this.statement = null;
        }
        if(this.connect != null && !this.connect.isClosed()){
            connect.close();
            this.connect = null;
        }
    }
    
}

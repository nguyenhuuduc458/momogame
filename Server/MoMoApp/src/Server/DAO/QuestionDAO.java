/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.DAO;

import utils.DTO.QuestionDTO;

/**
 *
 * @author nguyen huu duc
 */
public class QuestionDAO extends BaseDAO<QuestionDTO>{

    public QuestionDAO(String tableName, Class<QuestionDTO> clazz) {
        super(tableName, clazz);
    }

}

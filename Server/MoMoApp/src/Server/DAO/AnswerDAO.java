/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.DAO;

import utils.DTO.AnswerDTO;

/**
 *
 * @author nguyen huu duc
 */
public class AnswerDAO extends BaseDAO<AnswerDTO> {

    public AnswerDAO(String tableName, Class<AnswerDTO> clazz) {
        super(tableName, clazz);
    }
    
    public boolean deleteAnswerByQuestionId(String questionId) {
        DatabaseManager databaseManager = new DatabaseManager();
        
        try {
            String query = "DELETE FROM tblAnswer "
                         + "WHERE question_id=\'" + questionId + "\'";
            System.out.println(query);
            databaseManager.executeUpdate(query);
        }
        catch (Exception e) {
            return false;
        }
        
        return true;
    }
}

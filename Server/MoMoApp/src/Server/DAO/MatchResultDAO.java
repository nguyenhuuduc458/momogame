/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server.DAO;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.DTO.MatchResultDTO;
import utils.Models.StatiscalMatchModel;
import utils.Models.StatiscalWinModel;

/**
 *
 * @author nguyen huu duc
 */
public class MatchResultDAO extends BaseDAO<MatchResultDTO> {

    public MatchResultDAO(String tableName, Class<MatchResultDTO> clazz) {
        super(tableName, clazz);
    }

    public List<StatiscalMatchModel> findPlayerWithMaxMatchNumber() {
        DatabaseManager db = new DatabaseManager();
        List<StatiscalMatchModel> result = new ArrayList<>();
        String query = "select mem.firstname, mem.lastname, j.maxmatchnumber\n"
                + "from tblmember as mem,(select m.member_id as id, m.matchnumber as maxmatchnumber\n"
                + "                       from (select member_id, count(*) as matchnumber\n"
                + "                             from tblmatchresult\n"
                + "                             group by member_id) as m\n"
                + "                        where matchnumber = (select max(matchnumber)\n"
                + "                                             from (select member_id, count(*) as matchnumber\n"
                + "                                                   from tblmatchresult\n"
                + "                                                   group by member_id) as t)) as j\n"
                + "where mem.id = j.id";
        try {
            ResultSet rs = db.executeQuery(query);
            while (rs.next()) {
                String firstname = rs.getString(1);
                String lastname = rs.getString(2);
                int maxMatchNumber = rs.getInt(3);
                StatiscalMatchModel model = new StatiscalMatchModel(firstname, lastname, maxMatchNumber);
                result.add(model);
            }
            return result;
        } catch (Exception ex) {
            Logger.getLogger(MatchResultDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<StatiscalWinModel> findPlayerWithMaxWinNumber() {
        DatabaseManager db = new DatabaseManager();
        List<StatiscalWinModel> result = new ArrayList<>();
        String query = "select m.firstname, m.lastname, m.accumulate_point, t3.winmatch\n"
                + "from tblmember as m, (select member_id, winmatch\n"
                + "                      from (select member_id, count(*) winmatch\n"
                + "                          	from tblmatchresult\n"
                + "                          	where result = 1\n"
                + "                            group by member_id) as t1\n"
                + "                       where winmatch = (select max(winmatch) as maxwinmatch\n"
                + "                                         from (select member_id, count(*) winmatch\n"
                + "                                               from tblmatchresult\n"
                + "                                               where result = 1\n"
                + "                                               group by member_id) as t2)) as t3\n"
                + "where m.id = t3.member_id";
        try {
            ResultSet rs = db.executeQuery(query);
            while (rs.next()) {
                String firstname = rs.getString(1);
                String lastname = rs.getString(2);
                int accumulate_point = rs.getInt(3);
                int number_of_win = rs.getInt(4);
                StatiscalWinModel model = new StatiscalWinModel(firstname, lastname, accumulate_point, number_of_win);
                result.add(model);
            }
            return result;
        } catch (Exception ex) {
            Logger.getLogger(MatchResultDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

   
    
    public static void main(String[] args) {
        MatchResultDAO result = new MatchResultDAO("tblmatchresult", MatchResultDTO.class);
        List<StatiscalMatchModel> model1s = result.findPlayerWithMaxMatchNumber();
        List<StatiscalWinModel> model2s = result.findPlayerWithMaxWinNumber();

        for (StatiscalMatchModel model : model1s) {
            System.out.println(model.getFirstname() + " " + model.getLastname() + " " + model.getMatchnumber());
        }

        for (StatiscalWinModel model : model2s) {
            System.out.println(model.getFirstname() + " " + model.getLastname() + " " + model.getAccumulate_point() + " " + model.getNumberOfWin());
        }
    }
}

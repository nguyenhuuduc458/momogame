package Server.DAO;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.joda.time.DateTime;
import utils.DTO.MemberDTO;

public abstract class BaseDAO<T> {

    private String tableName;
    private Class<T> clazz;
    DatabaseManager databaseManager = new DatabaseManager();
    List<Field> fields;

    public BaseDAO(String tableName, Class<T> clazz) {
        this.tableName = tableName;
        this.clazz = clazz;
        this.fields = Arrays.asList(clazz.getDeclaredFields());
    }

    public T getById(String id) {
        String query = "SELECT * FROM " + tableName
                + " WHERE " + fields.get(0).getName().toString() + " = '" + id + "'";
        try {
            ResultSet resultSet = databaseManager.executeQuery(query);
            T dto = clazz.getConstructor().newInstance();
            for (Field field : fields) {
                field.setAccessible(true);
            }
            resultSet.next();
            for (Field field : fields) {
                String name = field.getName();
                if (int.class.isAssignableFrom(field.getType())) {
                    int value = resultSet.getInt(name);
                    field.set(dto, value);
                } else if (String.class.isAssignableFrom(field.getType())) {
                    String value = resultSet.getString(name);
                    field.set(dto, value);
                }
            }
            return dto;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public void delete(String id) {
        try {
            String query = "DELETE FROM " + tableName
                    + " WHERE " + fields.get(0).getName().toString() + "='" + id + "'";
            System.out.println(query);
            databaseManager.executeUpdate(query);

        } catch (Exception ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void update(T entity) {
        //CREATE MAP OF ENTITY
        Gson gson = new Gson();
        Map<String, Object> mapEntity = new Gson().fromJson(gson.toJson(entity), new TypeToken<HashMap<String, Object>>() {
        }.getType());
        System.out.println("MAP: " + mapEntity);

        //create query
        StringBuilder sb = new StringBuilder();

        sb.append("UPDATE " + tableName + " SET ");
        //SET VALUES
        for (Field f : fields) {
            //TRUE FALSE
            String name = f.getName();
            if (!name.equals("is_active")) {
                if (int.class.isAssignableFrom(f.getType())) {
                    sb.append(name + " = " + mapEntity.get(name) + ",");
                } else if (String.class.isAssignableFrom(f.getType())) {
                    sb.append(name + " = '" + mapEntity.get(name) + "',");
                } else {
                    sb.append(name + " = " + mapEntity.get(name) + ",");
                }
            }
        }
        //Remove ,
        sb = new StringBuilder(sb.substring(0, sb.length() - 1).toString());
        //WHERE CONDITION

        String key = fields.get(0).getName();
        sb.append(" WHERE " + key + " = '" + mapEntity.get(key) + "'");

        System.out.println(sb);
        try {
            databaseManager.executeUpdate(sb.toString());
        } catch (Exception ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void insert(T entity) {
        try {
            //CREATE MAP OF ENTITY
            Gson gson = new Gson();
            Map<String, Object> mapEntity = new Gson().fromJson(gson.toJson(entity), new TypeToken<HashMap<String, Object>>() {
            }.getType());
            System.out.println("MAP: " + mapEntity);
//        HashMap<String, Type> mapDataType = new HashMap<>();
//        for(Field f: fields)
//        {
//            mapDataType.put(f.getName(), f.getType());
//        }
//        System.out.println("DATATYPE "+mapDataType.toString());

            //create query
            StringBuilder sb = new StringBuilder();

            sb.append("INSERT INTO " + tableName
                    + " (");
            //insert attribute
            for (Field f : fields) {
                sb.append(f.getName() + ",");
            }
            
            //Remove ,
            sb = new StringBuilder(sb.substring(0, sb.length() - 1).toString());
            //insert Value
            sb.append(") Values (");
            for (Field f : fields) {
                //TRUE FALSE
                String name = f.getName();
                if (int.class.isAssignableFrom(f.getType())) {
                    sb.append(mapEntity.get(name).toString() + ",");
                } else if (String.class.isAssignableFrom(f.getType())) {
                    sb.append("'" + mapEntity.get(name) + "',");
                } else {
                    sb.append(mapEntity.get(name) + ",");
                }
            }
            //remove ,
            sb = new StringBuilder(sb.substring(0, sb.length() - 1).toString());
            sb.append(")");
            System.out.println(sb);
            databaseManager.executeUpdate(sb.toString());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public ConcurrentHashMap<String, T> getAll() {
        try {
            String query = "SELECT * FROM " + tableName;
            ConcurrentHashMap<String, T> map = new ConcurrentHashMap<>();
            ResultSet resultSet = null;
            try {
                resultSet = databaseManager.executeQuery(query);
            } catch (Exception e) {
                e.printStackTrace();
            }
            for (Field field : fields) {
                field.setAccessible(true);
            }

            while (resultSet.next()) {
                T dto = null;
                try {
                    dto = clazz.getConstructor().newInstance();
                    for (Field field : fields) {
                        String name = field.getName();

                        try {
                            if (int.class.isAssignableFrom(field.getType())) {
                                int value = resultSet.getInt(name);
                                field.set(dto, value);
                            } else if (String.class.isAssignableFrom(field.getType())) {
                                String value = resultSet.getString(name);
                                field.set(dto, value);
                            } else if (boolean.class.isAssignableFrom(field.getType())) {
                                boolean value = resultSet.getBoolean(name);
                                field.set(dto, value);
                            } 
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    map.put(resultSet.getString(fields.get(0).getName().toString()), dto);

                } catch (InstantiationException e) {
                } catch (IllegalAccessException e) {
                } catch (InvocationTargetException e) {
                } catch (NoSuchMethodException e) {
                }
            }
            return map;
        } catch (SQLException ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public List<T> getList() {
        try {
            String query = "SELECT * FROM " + tableName;
//            ConcurrentHashMap<String, T> map = new ConcurrentHashMap<>();
            List<T> list = new ArrayList<T>();

            ResultSet resultSet = null;
            try {
                resultSet = databaseManager.executeQuery(query);
            } catch (Exception e) {
                e.printStackTrace();
            }
            for (Field field : fields) {
                field.setAccessible(true);
            }

            while (resultSet.next()) {
                T dto = null;
                try {
                    dto = clazz.getConstructor().newInstance();
                    for (Field field : fields) {
                        String name = field.getName();

                        try {
                            if (int.class.isAssignableFrom(field.getType())) {
                                int value = resultSet.getInt(name);
                                field.set(dto, value);
                            } else if (String.class.isAssignableFrom(field.getType())) {
                                String value = resultSet.getString(name);
                                field.set(dto, value);
                            } else if (boolean.class.isAssignableFrom(field.getType())) {
                                boolean value = resultSet.getBoolean(name);
                                field.set(dto, value);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    list.add(dto);

                } catch (InstantiationException e) {
                } catch (IllegalAccessException e) {
                } catch (InvocationTargetException e) {
                } catch (NoSuchMethodException e) {
                }
            }
            return list;
        } catch (SQLException ex) {
            Logger.getLogger(BaseDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    public static void main(String[] args) {
        BaseDAO<MemberDTO> dao = new BaseDAO<MemberDTO>("tblmember", MemberDTO.class) {
        };
//        MemberDTO dto = new MemberDTO("id2", "user2", "fname", "lname", "sex", "1999/03/27", "password");
//        dao.insert(dto);
        List<MemberDTO> list = dao.getList();
        MemberDTO memberDTO = list.get(3);
        memberDTO.setIQpoint(1);
        memberDTO.setActive(false);
        dao.update(memberDTO);
    }
}

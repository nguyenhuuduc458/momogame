package Server;

import Server.BUS.MatchResultBUS;
import Server.BUS.MemberBUS;
import utils.DTO.MemberDTO;
import utils.Interfaces.Request;
import utils.Models.RequestModel;
import utils.Models.ResponseModel;
import utils.Encrypt.TripleDES;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.sun.mail.iap.ConnectionException;

import java.io.*;
import java.math.RoundingMode;
import java.net.ConnectException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.joda.time.DateTime;
import utils.DTO.AnswerDTO;
import utils.DTO.MatchResultDTO;
import utils.DTO.MatchResultDTO;
import utils.DTO.QuestionDTO;
import utils.Models.MatchResultModel;
import utils.Models.QuestionModel;
import utils.Models.RankingModel;
import utils.Models.UserDetailModel;

public class SessionProfile implements Runnable {

    private MemberDTO member;
    private Socket socket;
    private BufferedReader receive;
    private BufferedWriter send;
    private ServerMsgProcessor serverProcessor;
    private volatile boolean isLogin = false;

    private volatile boolean exit = false;
    private volatile LinkedHashMap<QuestionDTO, List<AnswerDTO>> setOfQuestions;// bộ câu hỏi cho một lượt chơi
    private volatile String competitor; // id của đối thủ
    private volatile boolean isGiveUp = false;

    // khởi tạo kết quả trận đấu cho user
    private volatile MatchResultDTO resultDTO;

    private volatile boolean isPlaying = false;
    private volatile boolean isParing = false;

    public SessionProfile(Socket socket, ServerMsgProcessor serverProcessor) {
        this.socket = socket;
        this.serverProcessor = serverProcessor;
    }

    /**
     * execute thread for this user
     */
    @Override
    public void run() {
        try {
            socket.setTcpNoDelay(true);

            receive = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            send = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

            // encrypt data receive from client
            String encryptData = receive.readLine();
            if (encryptData == null) {
                handleLogout();
            }

            // decrypt data 
            String decryptData = new TripleDES().decrypt(encryptData);

            dispatcher(decryptData);

        } catch (IOException e) {
            System.out.println("Server run: " + e.getMessage());
        }
    }

    /**
     * @param request: handle type of request receive from client
     */
    public void dispatcher(String request) {
        Gson gson = new Gson();
        RequestModel requestModel = gson.fromJson(request, RequestModel.class);
        switch (requestModel.getType()) {
            case LOGIN: {
                handleLogin(requestModel.getData());
                if (isLogin) {
                    while (true) {

                        try {
                            // encrypt data receive from client

                            this.socket.setSoTimeout(5 * 1000);

                            String encryptData = receive.readLine();

                            if (encryptData == null) {
                                System.out.println("Encrypt data == null");
                                handleLostConnect();
                                handleLogout();
                                break;
                            }

                            // decrypt data 
                            String decryptData = new TripleDES().decrypt(encryptData);
                            RequestModel rq = gson.fromJson(decryptData, RequestModel.class);

                            switch (rq.getType()) {
                                case PAIRING:
                                    handleParing(rq.getData());
                                    break;
                                case STOP_PAIRING:
                                    handleStopPairing(rq.getData());
                                    break;
                                case LOAD_QUESTION:
                                    handleLoadQuestion(rq.getData());
                                    break;
                                case ANSWER:
                                    handleAnswer(rq.getData());
                                    break;
                                case QUIT:
                                    handleQuit(rq.getData());
                                    break;
                                case GIVE_UP:
                                    handleQuit(rq.getData());
                                    handleGiveUp();
                                    break;
                                case SEARCH_RANK:
                                    handleSearchRank();
                                    break;
                                case TEST_IQ:
                                    handleTestIQ();
                                    break;
                                case SAVE_IQ_POINT:
                                    updateInfo(rq.getData());
                                    serverProcessor.handleUpdateInfo(rq.getData());
                                    break;
                                case GET_USER_DETAIL:
                                    handleLoadUserDetail(rq.getData());
                                    break;
                                case UPDATE_INFO:
                                    handleUpdateInfo(rq.getData());
                                    break;
                                case UPDATE_RULE:
                                    handleUpdateRule();
                                    break;
                                case LOGOUT:
                                    handleLogout();
                                    break;
                                default:
//                                    System.out.println(rq.getData());
                            }
                            if (rq.getType().equals(Request.LOGOUT)) {
                                break;
                            }

                        } catch (SocketTimeoutException e) {
                            // check whether user lost internet connection
                            System.out.println("SocketTimeout");
                            handleLostConnect();
                            handleLogout();
                            break;
                        } catch (SocketException e) {
                            System.out.println("SocketException");
                            handleLostConnect();
                            handleLogout();
                            break;
                        } catch (IOException e) {
                            handleLostConnect();
                            handleLogout();
                            break;
                        } catch (NullPointerException e) {
                        } catch (UnknownError e) {
                            e.printStackTrace();
                        }
                    }
                }
                break;
            }
            case SEND_OTP:
                handleSendOtp(requestModel.getData());
                closeConnection();
                break;
            case REGISTER:
                handleRegister(requestModel.getData());
                closeConnection();
                break;
            case GET_ONLINE_USER:
                handleGetOnlineUser();
                break;
            default:
                System.out.println("STILL_ALIVE");
        }
    }

    public void sendMessage(ResponseModel responseModel) {
        Gson gson = new Gson();
        // encypt data and send to client
        String data = new TripleDES().encrypt(gson.toJson(responseModel));
        try {
            if (socket.isConnected()) {
                send.write(data);
                send.newLine();
                send.flush();
            } else {
                System.out.println("Không kết nối được với client " + member.getFullName());
            }
        } catch (IOException e) {

        }
    }

    public Socket getSocket() {
        return this.socket;
    }

    public void setMember(MemberDTO member) {
        this.member = member;
    }

    public MemberDTO getMember() {
        return member;
    }

    public SessionProfile getSessionProfile() {
        return this;
    }

    public MatchResultDTO getResultDTO() {
        return resultDTO;
    }

    // cập nhật mã cho kết quả trận đấu
    public void setMatchId(String matchId) {
        Timestamp ts = new Timestamp(System.currentTimeMillis());
        resultDTO = new MatchResultDTO(UUID.randomUUID().toString(), member.getId(), matchId, String.valueOf(ts));
    }

    // lưu giữ lại id của đối thủ
    public void setCompetitor(String id) {
        this.competitor = id;
    }

    // tải từng câu hỏi lên cho người chơi
    public QuestionModel loadNewQuestion(int position) {
        try {
            int flag = 0;
            for (QuestionDTO question : setOfQuestions.keySet()) {
                if (flag == position) {
                    List<AnswerDTO> answers = setOfQuestions.get(question);
                    QuestionModel model = new QuestionModel();
                    model.setQuestion(question);
                    model.setAnswer(answers);
                    model.findRighAnswer();
                    System.out.println("Load question số " + position + " trong luồng của user " + member.getFullName() + " nội dung " + question.getContent());
                    return model;
                } else {
                    flag++;
                }
            }
        } catch (NullPointerException e) {

        } catch (Exception e) {

        }
        return null;
    }

    // lưu bộ câu hỏi trong luồng của từng user
    public void createSetOfQuestion(LinkedHashMap<QuestionDTO, List<AnswerDTO>> setOfQuestions) {
        this.setOfQuestions = setOfQuestions;
    }

    public void updateInfo(String data) {
        if (data != null) {
            setMember(new Gson().fromJson(data, MemberDTO.class));
        }
    }

//    public void sendHeartBeat() {
//        try {
////            send.write("Send check");
////            send.newLine();
////            send.flush();
//            socket.setKeepAlive(true);
//            socket.sendUrgentData(0xff);
//            socket.setOOBInline(true);
//
////            sendMessage(new ResponseModel(Request.CHECK_CONNECTION, true, "CheckConnect"));
//            System.out.println("Send check");
//        } catch (IOException e) {
//            handleLostConnect();
//
//            //remove user when pairing request and internet going off
//            serverProcessor.PENDING_QUEUE.remove(member.getId());
//        }
//    }
//    public boolean isConnected() {
//        boolean isAlive = false;
//        try {
//            socket.sendUrgentData(0xff);
//            socket.setOOBInline(true);
//            isAlive = true;
//        } catch (IOException e) {
//            isAlive = false;
//        }
//        return isAlive;
//    }
    public void handleLostConnect() {
        if (isPlaying) {
//            int currentPoint = resultDTO.getTotal_point();
            handleQuit(String.valueOf("0") + ";" + String.valueOf(false));
            handleGiveUp();
            System.out.println("Member: " + member.getFullName() + " lost internet connection and still playing");
        } else if (isParing) {
            serverProcessor.removeUserFromPending(member.getId());
        } else {
            System.out.println("Member: " + member.getFullName() + " lost internet connection, not playing and not pairing");
        }
    }

    public void handleParing(String data) {
        serverProcessor.transitOnlineUserToPending(data); // data: memberDTO;
        isParing = true;
    }

    public void handleStopPairing(String data) {
        for (String key : ServerMsgProcessor.PENDING_QUEUE) {
            if (key.equals(data)) {
                ServerMsgProcessor.PENDING_QUEUE.remove(key);
                System.out.println("User: " + data + " được remove khỏi pending queue");
            }
        }

        //cacel 
        ResponseModel message2 = new ResponseModel(Request.STOP_PAIRING, true, "");
        serverProcessor.broadcastMessageToOnlineUser(message2, getSessionProfile());
    }

    public void handleLoadQuestion(String data) throws SocketException {
        String[] input = data.split(";");
        int questionPosition = Integer.parseInt(input[0]);
        int currentPoint = Integer.parseInt(input[1]); // current point of user after answer each question

        // save currentPoint
        resultDTO.setTotal_point(currentPoint);

        //send question to client 
        sendMessage(new ResponseModel(Request.LOAD_QUESTION,
                true,
                new Gson().toJson(loadNewQuestion(questionPosition))));

        // cập nhật biến isPlaying  = true đê liên tục kiểm tra kết nối của client đến 2 người chơi 
        isPlaying = true;

        //check socket alive
//        this.socket.setSoTimeout(5 * 1000);
    }

    public void handleAnswer(String data) {
        // chuyển câu trả lời cho đối thủ
        serverProcessor.broadcastMessageToCompetitor(competitor, new ResponseModel(Request.ANSWER, true, data));
    }

    public void handleQuit(String data) {
        String[] input = data.split(";");
        int point = new Gson().fromJson(input[0], new TypeToken<Integer>() {
        }.getType());
        boolean isWinnner = new Gson().fromJson(input[1], new TypeToken<Boolean>() {
        }.getType());

        // update new point in match result
        resultDTO.setTotal_point(point);

        // update accumulate point of user
        int oldAccumulatePoint = member.getAccumulatePoint();
        System.out.println("Điểm trước khi chơi của user" + member.getFullName() + " " + oldAccumulatePoint);
        int newAccumulatePoint = resultDTO.getTotal_point() + oldAccumulatePoint;
        System.out.println("Điểm sau khi chơi của user" + member.getFullName() + " " + newAccumulatePoint);
        member.setAccumulatePoint(newAccumulatePoint);

        // save new user point to db
        serverProcessor.handleUpdateInfo(new Gson().toJson(member));

        // save match result to db
        resultDTO.setResult(isWinnner);
        resultDTO.setTotal_point(point);
        serverProcessor.saveMatchResult(resultDTO);

        isPlaying = false;
        isParing = false;

    }

    public void handleGiveUp() {
        //broadcast message to inform give up for opponenent
        ResponseModel message = new ResponseModel(Request.GIVE_UP, true, "Đối thủ " + member.getFullName() + " đã thoát khỏi trận đấu" + ";" + String.valueOf(resultDTO.getTotal_point()));
        serverProcessor.broadcastMessageToCompetitor(competitor, message);

        //cacel 
        ResponseModel message2 = new ResponseModel(Request.GIVE_UP_OK, true, "");
        serverProcessor.broadcastMessageToOnlineUser(message2, getSessionProfile());

    }

    public void handleSearchRank() {
        MemberBUS service = new MemberBUS();
        List<RankingModel> ranks = service.getRank();
        ResponseModel responseModel;
        Gson gson = new Gson();
        if (!ranks.isEmpty()) {
            responseModel = new ResponseModel(Request.SEARCH_RANK, true, gson.toJson(ranks));
        } else {
            responseModel = new ResponseModel(Request.SEARCH_RANK, false, "Không tìm thấy dữ liệu trong db");
        }
        sendMessage(responseModel);
        System.out.println("Gửi dữ liệu search rank");
    }

    public void handleTestIQ() {
        ResponseModel model;
        Gson gson = new Gson();
        if (ServerMsgProcessor.IQ_TEST_QUESTION.size() > 0) {
            model = new ResponseModel(Request.TEST_IQ, true, gson.toJson(ServerMsgProcessor.IQ_TEST_QUESTION));
        } else {
            model = new ResponseModel(Request.TEST_IQ, false, "Trang chủ IQ đang được bảo trì!");
        }

        sendMessage(model);
    }

    public void handleLoadUserDetail(String userId) { //data: userId;
        // get user info 
        MemberBUS memberBUS = new MemberBUS();
        MemberDTO member = memberBUS.getMemberById(userId);

        // get total match of user
        // get win and loss number
        MatchResultBUS matchResultBUS = new MatchResultBUS();
        List<MatchResultDTO> matchResultList = matchResultBUS.getList();
        List<MatchResultModel> matchResultOfUser = new ArrayList<>();
        int totalMatch = 0;
        int win = 0;
        int loss = 0;
        for (MatchResultDTO result : matchResultList) {
            if (result.getMember_id().equals(userId)) {
                totalMatch++;
                matchResultOfUser.add(result.mappingToModel());
                if (result.isResult()) {
                    win++;
                } else {
                    loss++;
                }
            }
        }

        //caculate win and loss rate
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.CEILING);
        double winRate;
        double lossRate;
        if (totalMatch != 0) {
            winRate = (win * 100) * 1.0 / totalMatch;
            lossRate = (loss * 100) * 1.0 / totalMatch;
        } else {
            winRate = lossRate = 50;
        }

        //sort result by datetime
        Collections.sort(matchResultOfUser);

        // find chain of win and chain of loss
        int maxChainOfWin = findMaxChainOfWin(matchResultOfUser);
        int maxChainOfLoss = findMaxChainOfLoss(matchResultOfUser);

        // set model user detail
        UserDetailModel model = new UserDetailModel();
        model.setMember(member);
        model.setTotalMatch(totalMatch);
        model.setWinRate(df.format(winRate) + "%");
        model.setLossRate(df.format(lossRate) + "%");
        model.setMatchResult(matchResultOfUser);
        model.setMaxChainOfWin(String.valueOf(maxChainOfWin));
        model.setMaxChainOfLoss(String.valueOf(maxChainOfLoss));

        //send to client
        sendMessage(new ResponseModel(Request.GET_USER_DETAIL, true, new Gson().toJson(model)));
    }

    public int findMaxChainOfWin(List<MatchResultModel> results) {

        int max = 0;
        int chainWin = 0;
        for (int i = 0; i < results.size(); i++) {
            if (results.get(i).isResult()) {
                chainWin++;
            } else {
                chainWin = 0;
            }
            if (chainWin >= max) {
                max = chainWin;
            }
        }
        return max;
    }

    public int findMaxChainOfLoss(List<MatchResultModel> results) {
        int max = 0;
        int chainLoss = 0;
        for (int i = 0; i < results.size(); i++) {
            if (!results.get(i).isResult()) {
                chainLoss++;
            } else {
                chainLoss = 0;
            }
            if (chainLoss >= max) {
                max = chainLoss;
            }
        }
        return max;
    }

    public void handleUpdateInfo(String data) {
        updateInfo(data);
        ResponseModel responseModel = serverProcessor.handleUpdateInfo(data);
        serverProcessor.broadcastMessageToOnlineUser(responseModel, getSessionProfile());
    }

    public void handleUpdateRule() {
        ResponseModel updateResponse = serverProcessor.updateRuleInServerAndBroadcastToClient();
        sendMessage(updateResponse);
    }

    public void handleRegister(String data) {
        ResponseModel responseModel = serverProcessor.handleRegister(data);
        serverProcessor.broadcast(responseModel, getSessionProfile());
        serverProcessor.transitUndefineUserToOnline(null, getSessionProfile());
    }

    public void handleSendOtp(String data) {
        ResponseModel responseModel = serverProcessor.handleSendOTP(data);
        serverProcessor.broadcast(responseModel, getSessionProfile());
        serverProcessor.transitUndefineUserToOnline(null, getSessionProfile());
    }

    public void handleLogin(String data) {
        ResponseModel responseModel = serverProcessor.handleLogin(data);
        if (responseModel.isStatus() && responseModel.getType().equals(Request.LOGIN)) {
            // cập nhật thông tin người dùng trong session profile 
            updateInfo(responseModel.getData());
            System.out.println("User: " + member.getFullName() + " is online");
            isLogin = true;
        }

        serverProcessor.broadcast(responseModel, getSessionProfile());
        serverProcessor.transitUndefineUserToOnline(responseModel.isStatus() ? getMember().getId() : null, getSessionProfile());

        if (!isLogin) {
            closeConnection();
        }
    }

    public void handleLogout() {
        try {
            System.out.println("Member " + member.getFullName() + " logout.");
            serverProcessor.handleLogout(member.getId());
            closeConnection();
        } catch (NullPointerException e) {
        }

    }

    public void closeConnection() {
        try {
            if (send != null) {
                send.close();
            }
            if (receive != null) {
                receive.close();
            }
            if (!socket.isClosed()) {
                socket.close();
            }
            System.out.println("Client " + socket.getRemoteSocketAddress() + " disconnect from server...");
        } catch (IOException ex) {
            System.out.println("Error closing: " + ex.getMessage());
        } catch (NullPointerException e) {
            // do nothing
        }
    }

    private void handleGetOnlineUser() {
        int onlineUser = serverProcessor.getOnlineUser();
        sendMessage(new ResponseModel(Request.GET_ONLINE_USER, true, String.valueOf(onlineUser)));
        serverProcessor.transitUndefineUserToOnline(null, getSessionProfile());
        if (!isLogin) {
            closeConnection();
        }
    }
}

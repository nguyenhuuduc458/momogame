package Server;

import Server.BUS.MatchBUS;
import Server.BUS.MatchResultBUS;
import Server.BUS.MemberBUS;
import Server.BUS.QuestionAndAnswerBUS;
import Server.BUS.RuleBUS;
import utils.DTO.MemberDTO;
import utils.Interfaces.Request;
import utils.Models.Account;
import utils.Models.ResponseModel;
import utils.Common.Email;
import utils.Common.StringExtension;
import utils.Env.Const;
import com.google.gson.Gson;
import utils.DTO.MatchDTO;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import utils.DTO.MatchResultDTO;
import utils.Models.IQTestModel;

public class ServerMsgProcessor extends Thread {

    public static volatile ConcurrentHashMap<String, SessionProfile> SESSION_POOL = new ConcurrentHashMap<>();
    public static volatile ConcurrentLinkedQueue<String> PENDING_QUEUE = new ConcurrentLinkedQueue<>();
    private static volatile ConcurrentLinkedQueue<SessionProfile> UNDEFINED_QUEUE = new ConcurrentLinkedQueue<>();
    public static volatile LinkedHashMap<String, IQTestModel> IQ_TEST_QUESTION = new LinkedHashMap<>();

    public ServerMsgProcessor() {
        MemberBUS service = new MemberBUS();

    }

    /**
     * @Main listening and accept client when connect to server Create thread
     * for every client
     */
    @Override
    public void run() {
        try {
            // listening port
            System.out.println("==========Server side=========");
            InetAddress inetAddress = InetAddress.getLocalHost();
            ServerSocket serverSocket = new ServerSocket(Const.PORT);
            System.out.println("Server opened at: " + inetAddress.getHostAddress());
            System.out.println("Server start on port: " + serverSocket.getLocalPort());
            System.out.println("Waiting for client...");

            // auto run pairing player after 1 seconds
            pairing();

            //auto load iq test
            loadIQQuestion();

            // accept connection
            ExecutorService service = Executors.newFixedThreadPool(20);
            while (true) {
                Socket socket = serverSocket.accept();
                System.out.println("Client " + socket.getRemoteSocketAddress() + " connected to server...");
                String ip = (((InetSocketAddress) socket.getRemoteSocketAddress()).getAddress()).toString().replace("/", "");
                System.out.println(ip);
                SessionProfile userThread = new SessionProfile(socket, this);
                UNDEFINED_QUEUE.offer(userThread);
                service.execute(userThread);
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    static boolean isPendingEmpty() {
        return PENDING_QUEUE.isEmpty();
    }

    public void broadcast(ResponseModel data, SessionProfile sessionProfile) {
        UNDEFINED_QUEUE.stream().filter((session) -> (session == sessionProfile)).forEach((session) -> {
            session.sendMessage(data);
        });
    }

    public void broadcastMessageToOnlineUser(ResponseModel data, SessionProfile sessionProfile) {
        ExecutorService service = Executors.newCachedThreadPool();
        service.execute(() -> {
            SESSION_POOL.keySet().stream().filter((session) -> (SESSION_POOL.get(session).equals(sessionProfile))).forEach((session) -> {
                sessionProfile.sendMessage(data);
                System.out.println("Send message " + data.getType() + " to online user: " + SESSION_POOL.get(session).getMember().getFullName());
            });
        });
    }

    public void broadcastMessageToCompetitor(String competitorId, ResponseModel responseModel) {
        ExecutorService service = Executors.newCachedThreadPool();
        service.execute(() -> {
            SESSION_POOL.keySet().stream().filter((key) -> (key.equals(competitorId))).forEach((key) -> {
                SESSION_POOL.get(key)
                        .sendMessage(responseModel);
            });
        });
        service.shutdown();
    }

    public void transitUndefineUserToOnline(String memberId, SessionProfile sessionProfile) {
        UNDEFINED_QUEUE.forEach(userThread -> {
            if (userThread == sessionProfile) {
                if (memberId != null) {
                    SESSION_POOL.put(memberId, userThread);
                }
                UNDEFINED_QUEUE.remove(userThread);
            }
        });
    }

    public void transitOnlineUserToPending(String data) {
        ExecutorService service = Executors.newCachedThreadPool();
        service.execute(() -> {
            MemberDTO member = new Gson().fromJson(data, MemberDTO.class);
            System.out.println(member);
            for (String ssid : SESSION_POOL.keySet()) {
                if (SESSION_POOL.get(ssid).getMember().getId().equals(member.getId())) {
                    PENDING_QUEUE.add(ssid);
                    System.out.println("user:  " + member.getFullName() + " được add vào pending queue");
                    System.out.println("Số lượng user trong pending queue: " + PENDING_QUEUE.size());
                }
            }
        });
    }

    public void removeUserFromPending(String memberId) {
        for (String key : PENDING_QUEUE) {
            if (key.equalsIgnoreCase(memberId)) {
                PENDING_QUEUE.remove(key);
                System.out.println("User id: " + key + " được remove khỏi pending queue");
            }
        }
    }

    public void pairing() {
        ExecutorService pairingExecutorService = Executors.newSingleThreadExecutor();
        pairingExecutorService.execute(() -> {
            while (true) {
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(ServerMsgProcessor.class.getName()).log(Level.SEVERE, null, ex);
                }
                while (PENDING_QUEUE.size() >= 2) {
                    System.out.println("dang ghep cap");
                    String player1 = PENDING_QUEUE.poll();
                    String player2 = PENDING_QUEUE.poll();

                    // gửi tin nhắn thông báo đối thủ cho hai người chơi
                    broadcastMessageToOnlineUser(new ResponseModel(Request.PAIRING,
                            true,
                            new Gson().toJson(SESSION_POOL.get(player2).getMember())),
                            SESSION_POOL.get(player1));

                    broadcastMessageToOnlineUser(new ResponseModel(Request.PAIRING,
                            true,
                            new Gson().toJson(SESSION_POOL.get(player1).getMember())),
                            SESSION_POOL.get(player2));
                    String matchId = UUID.randomUUID().toString();

                    //tạo mã trận đấu cho 2 người chơi
                    setMatchId(player1, matchId);
                    setMatchId(player2, matchId);

                    //truy cập dưới server lấy bộ câu hỏi cho người chơi
                    final QuestionAndAnswerBUS service = new QuestionAndAnswerBUS();
                    service.createSetOfQuestions();
                    SESSION_POOL.get(player1)
                            .getSessionProfile()
                            .createSetOfQuestion(service.getSetOfQuestions());
                    SESSION_POOL.get(player2)
                            .getSessionProfile()
                            .createSetOfQuestion(service.getSetOfQuestions());

                    // cập nhật mã của đối thủ trong luồng của người chơi
                    updateCompetitorInfo(player1, player2);
                    updateCompetitorInfo(player2, player1);

                    // tạo một trận đấu và lưu xuống csdl
                    MatchBUS matchBUS = new MatchBUS();
                    MatchDTO match = new MatchDTO(matchId, "Đấu giao hữu");
                    matchBUS.addItem(match);
                }
            }
        });

    }

    public void updateCompetitorInfo(String userId, String competitorId) {
        ExecutorService service = Executors.newFixedThreadPool(2);
        service.execute(() -> {
            SESSION_POOL.get(userId)
                    .setCompetitor(competitorId);
        });
        service.shutdown();
    }

    // check exit username 
    public boolean isExistUsername(String username) {
        MemberBUS service = new MemberBUS();
        ConcurrentHashMap<String, MemberDTO> members = service.getAll();
        for (String key : members.keySet()) {
            if (members.get(key).getUsername().equals(username)) {
                return true;
            }
        }
        return false;
    }

    public ResponseModel handleSendOTP(String username) {
        if (!isExistUsername(username)) {
            ResponseModel responseModel;
            ExecutorService service = Executors.newSingleThreadExecutor();
            Future<ResponseModel> result = service.submit(() -> {
                char[] otpGenerate = StringExtension.generateOTP(4);
                Email email = new Email.Builder(Const.EMAIL_SENDER, username, Const.PASSWORD)
                        .setSubject("OTP verify")
                        .setContent(StringExtension.getOTPTemplate(username, otpGenerate))
                        .build();
                if (email.send()) {
                    return new ResponseModel(Request.SEND_OTP, true, String.valueOf(otpGenerate));
                } else {
                    return new ResponseModel(Request.SEND_OTP, false, "Có lỗi sảy ra khi gửi email");
                }
            });
            try {
                responseModel = result.get();
                return responseModel;
            } catch (InterruptedException | ExecutionException ex) {
                System.out.println("Email không được gửi thành công vui lòng kiểm tra lại internet");
                return null;
            }
        } else {
             return new ResponseModel(Request.SEND_OTP, false, "Email đã được đăng kí cho một tài khoản khác.");
        }
    }

    public ResponseModel handleLogin(String account) {
        ResponseModel responseModel;
        ExecutorService service = Executors.newSingleThreadExecutor();
        Future<ResponseModel> result = service.submit(() -> {
            Account user = new Gson().fromJson(account, Account.class);
            MemberBUS serviceBUS = new MemberBUS();
            MemberDTO member = serviceBUS.loginCheck(user.getUsername(), user.getPassword());
            if (member != null) {
                if (isOnlineUser(member.getId())) {
                    return new ResponseModel(Request.LOGIN, false, "Người dùng hiện đang online trên một thiết bị khác");
                } else if (!member.isActive()) {
                    return new ResponseModel(Request.LOGIN, false, "Tài khoản bị khóa");
                }
                return new ResponseModel(Request.LOGIN, true, new Gson().toJson(member));
            }
            return new ResponseModel(Request.LOGIN, false, "Đăng nhập thất bại thông tin tài khoản không đúng");
        });
        try {
            responseModel = result.get();
            return responseModel;
        } catch (InterruptedException | ExecutionException ex) {
            return null;
        }
    }

    public ResponseModel handleRegister(String data) {
        Gson gson = new Gson();
        MemberDTO memberDTO = gson.fromJson(data, MemberDTO.class);
        MemberBUS service = new MemberBUS();

        if (memberDTO != null) {
            if (service.addItem(memberDTO)) {
                return new ResponseModel(Request.REGISTER, true, "Chúc mừng bạn đã đăng kí tài khoản thành công");
            } else {
                return new ResponseModel(Request.REGISTER, false, "Đăng kí tài khoản thất bại");
            }
        }
        return new ResponseModel(Request.REGISTER, false, "Tài khoản không chứa nội dung hợp lệ vui lòng đăng kí lại");
    }

    public ResponseModel handleUpdateInfo(String data) {
        MemberDTO member = new Gson().fromJson(data, MemberDTO.class);
        MemberBUS service = new MemberBUS();
        if (service.updateItem(member)) {
            return new ResponseModel(Request.UPDATE_INFO, true, "Cập nhật thông tin thành công");
        } else {
            return new ResponseModel(Request.UPDATE_INFO, false, "Cập nhật thông tin thất bại");
        }
    }

    public void handleLogout(String userId) {
        SESSION_POOL.remove(userId);
        System.out.println("Online user: " + SESSION_POOL.size());
    }

    public ResponseModel updateRuleInServerAndBroadcastToClient() {
        // udpate rule in server
        RuleBUS service = new RuleBUS();
        service.updateRule();

        //broadcast to client
        ResponseModel responseModel = new ResponseModel(Request.UPDATE_RULE,
                true,
                new Gson().toJson(service.getRules())
        );
        return responseModel;
    }

    public void setMatchId(String userId, String matchId) {
        ExecutorService service = Executors.newCachedThreadPool();
        service.execute(() -> {
            SESSION_POOL.get(userId)
                    .getSessionProfile()
                    .setMatchId(matchId);
        });
        service.shutdown();
    }

    // lưu kết quả trận đấu xuống server
    public void saveMatchResult(MatchResultDTO result) {
        System.out.println("==============SAVE MATCH RESULT=============");
        MatchResultBUS service = new MatchResultBUS();
        if (service.addItem(result)) {
            System.out.println("Thêm kết quả trận đấu thành công");
        } else {
            System.out.println("Thêm kết quả trận đấu thất bại");
        }
    }

    public boolean isOnlineUser(String userId) {
        return SESSION_POOL.containsKey(userId);

    }

    public int getOnlineUser() {
        return SESSION_POOL.size();
    }

    // tải bộ câu hỏi iq từ trang web về
    public void loadIQQuestion() {
        ExecutorService service = Executors.newSingleThreadExecutor();
        service.execute(() -> {
            while (true) {

                try {
                    String userAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/43.0.2357.130 Safari/537.36";
                    Document doc = Jsoup.connect("https://hoaky68.com/chude/iq/kiem-tra-iq/")
                            .userAgent(userAgent)
                            .ignoreHttpErrors(true)
                            .ignoreContentType(true)
                            .get();

                    // get question text
                    List<String> questions = new ArrayList<>();
                    Elements questionTexts = doc.getElementsByTag("p");
                    for (Element questionText : questionTexts) {
                        Element q = questionText.tagName("strong");
                        if (q.text().contains("Câu") && q.text().contains(":")) {
                            questions.add(q.text());
                        }
                    }

                    // get question and answer
                    List<String> questionImages = new ArrayList<>();
                    Elements figureTagHtml = doc.getElementsByTag("figure");
                    Elements imgLinkUrlHtml = figureTagHtml.select("img");
                    for (Element url : imgLinkUrlHtml) {
                        questionImages.add(url.absUrl("src"));
                    }

                    // get answers
                    List<String> answers = new ArrayList<>();
                    Elements answersHtml = doc.getElementsByClass("divNoiDung");
                    for (Element element : answersHtml) {
                        answers.add(element.text().split(":")[1]);
                    }

                    // create set of iq test
                    for (int i = 0; i < questions.size(); i++) {
                        IQTestModel model = new IQTestModel();
                        model.setContent(questions.get(i));
                        model.setImgQuestionUrl(questionImages.get(i));
                        model.setAnswer(answers.get(i));
                        IQ_TEST_QUESTION.put(UUID.randomUUID().toString(), model);
                    }

                    if (IQ_TEST_QUESTION.size() > 0) {
                        System.out.println("Khởi tạo thành công bộ câu hỏi iq");
                    }

                } catch (IOException e) {
                    try {
                        System.out.println("Không truy cập được vào trang web để lấy bộ câu hỏi iq");
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(ServerMsgProcessor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                if(IQ_TEST_QUESTION.size() > 0) {
                    try {
                        Thread.sleep(5 * 3600 * 1000); // auto reload after 5 hour
                    } catch (InterruptedException ex) {
                        Logger.getLogger(ServerMsgProcessor.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    if (IQ_TEST_QUESTION.size() > 0) {
                        IQ_TEST_QUESTION.clear();
                    }
                }
            }
        });
        service.shutdown();
    }
}

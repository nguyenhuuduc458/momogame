/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Env;


/**
 * @author nguyen huu duc
 */
public class Const {

    /*SERVER CONFIG*/
    public static String HOSTNAME = "localhost";
    public static int PORT = 10000;

    /*CIPHER PARAMETER*/
    public static final String SALT = "HG58YZ3CR9";
    public static final String CIPHER_INSTANCE = "DESede/CBC/PKCS5Padding";
    public static final String MD5_INSTANCE = "md5";
    public static final String SECRET_KEY = "DESede";

    /*EMAIL PARAMETER*/
    public static final String EMAIL_SENDER = "nguyentranvan1041999@gmail.com";
    public static final String PASSWORD = "Noname1999@";

    /*GAME RULE*/
    public static volatile int WAIT_JOIN_INTERVAL = 60;
    public static volatile int TIME_ANSWER = 10;
    public static volatile int QUESTION_NUMBER = 5;
    public static volatile int POINT_PLUS = 80;
    

    /*NOTIFICATION*/
    public static final String LOGIN_SUCCESS = "Đăng nhập thành công";
    public static final String LOGIN_FAIL = "Đăng nhập thất bại vui lòng kiểm tra lại thông tin đăng nhập";
    
    
}

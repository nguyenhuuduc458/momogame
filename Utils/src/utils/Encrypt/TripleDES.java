/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Encrypt;

import utils.Env.Const;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Base64;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author nguyen huu duc
 */
public class TripleDES {

      public String encrypt(String message){
        try {
            String encryptedString = "";
            final MessageDigest md = MessageDigest.getInstance(Const.MD5_INSTANCE);
            final byte[] digestOfPassword = md.digest(Const.SALT.getBytes("utf-8"));

            final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
            for (int j = 0, k = 16; j < 8;) {
                keyBytes[k++] = keyBytes[j++];
            }

            final SecretKey key = new SecretKeySpec(keyBytes, Const.SECRET_KEY);
            final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
            final Cipher cipher = Cipher.getInstance(Const.CIPHER_INSTANCE);
            cipher.init(Cipher.ENCRYPT_MODE, key, iv);

            final byte[] plainTextBytes = message.getBytes("utf-8");
            final byte[] cipherText = cipher.doFinal(plainTextBytes);
            encryptedString = Base64.getEncoder().encodeToString(cipherText);

            return encryptedString;
        } catch (Exception e) {
            System.out.println("Error when encrypt data: " + e.getMessage());
        }
        return null;
    }

    public String decrypt(String encryptedString) {
        try {
            final MessageDigest md = MessageDigest.getInstance(Const.MD5_INSTANCE);
            final byte[] digestOfPassword = md.digest(Const.SALT.getBytes("utf-8"));

            final byte[] keyBytes = Arrays.copyOf(digestOfPassword, 24);
            for (int j = 0, k = 16; j < 8;) {
                keyBytes[k++] = keyBytes[j++];
            }

            final SecretKey key = new SecretKeySpec(keyBytes, Const.SECRET_KEY);
            
            final IvParameterSpec iv = new IvParameterSpec(new byte[8]);
            final Cipher decipher = Cipher.getInstance(Const.CIPHER_INSTANCE);
            decipher.init(Cipher.DECRYPT_MODE, key, iv);
            byte [] byteDecrypted = Base64.getDecoder().decode(encryptedString);
            final byte[] plainText = decipher.doFinal(byteDecrypted);

            return new String(plainText, "UTF-8");
        } catch (Exception e) {
//            System.out.println("Error when decrypt data: " + e.getMessage());
        }
        return null;
    }
}

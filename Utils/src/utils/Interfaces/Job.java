/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Interfaces;

/**
 *
 * @author nguyen huu duc
 */
public interface Job {
    boolean doDuringCountdown();
    void doAfterCountdown();
}

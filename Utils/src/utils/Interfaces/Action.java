/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Interfaces;

import java.io.BufferedReader;
import java.io.BufferedWriter;

/**
 *
 * @author nguyen huu duc
 */
public interface Action {
    void executeAction(BufferedWriter send, BufferedReader receive);
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Interfaces;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;

/**
 *
 * @author nguyen huu duc
 */
public interface HandleMessage extends Action {
    void sendMessage(BufferedWriter send, String message) throws IOException;
    void receiveMessage(BufferedReader receive) throws IOException;
}

package utils.Interfaces;

import utils.DTO.AnswerDTO;

public enum Request {

    LOGIN,
    UPDATE_INFO,
    SEND_OTP,
    REGISTER,
    UPDATE_RULE,
    
    PAIRING,
    STOP_PAIRING,
    
    TEST_IQ,
    SAVE_IQ_POINT,
    SEARCH_RANK,
    GET_USER_DETAIL,
    CHECK_CONNECTION,
    GET_ONLINE_USER,
    
    LOGOUT, // KHI NGƯỜI DÙNG ĐĂNG XUẤT
    ANSWER, // KHI NGƯỜI DÙNG TRẢ LỜI XONG MỘT CÂU 
    LOAD_QUESTION, // KHI ĐỐI THỦ TRẢ LỜI XONG CÂU HỎI VÀ LOAD LÊN TIẾP CÂU MỚI
    KEEP_PLAYING, // TIẾP TỤC TRẬN ĐẤU
    GIVE_UP, // ĐỐI THỦ TỪ BỎ CUỘC CHƠI
    GIVE_UP_OK, // CHAP NHAN YEU CAU HUY
    QUIT // HẾT MỘT TRẬN ĐẤU
    
    
}

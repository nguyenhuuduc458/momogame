/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Models;

/**
 *
 * @author nguyen huu duc
 */
public class RankingModel {
    private String id;
    private String fullname;
    private int accumulatePoint;
    private int rank;

    public RankingModel() {
    }

    public RankingModel(String id, String fullname, int accumulatePoint, int rank) {
        this.id = id;
        this.fullname = fullname;
        this.accumulatePoint = accumulatePoint;
        this.rank = rank;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public int getAccumulatePoint() {
        return accumulatePoint;
    }

    public void setAccumulatePoint(int accumulatePoint) {
        this.accumulatePoint = accumulatePoint;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }
    
    
}

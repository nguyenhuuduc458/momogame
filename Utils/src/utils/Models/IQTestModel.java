/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Models;

/**
 *
 * @author nguyen huu duc
 */
public class IQTestModel {
    private String content;
    private String imgQuestionUrl;
    private String answer;

    public IQTestModel() {
    }

    public IQTestModel(String content, String imgQuestionUrl,String answer) {
        this.content = content;
        this.imgQuestionUrl = imgQuestionUrl;
        this.answer = answer;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImgQuestionUrl() {
        return imgQuestionUrl;
    }

    public void setImgQuestionUrl(String imgQuestionUrl) {
        this.imgQuestionUrl = imgQuestionUrl;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
    
    
}

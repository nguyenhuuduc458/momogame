package utils.Models;

import utils.Interfaces.Request;

public class RequestModel {
    private Request type;
    private String data;

    public RequestModel() {}

    public RequestModel(Request type, String data) {
        this.type = type;
        this.data = data;
    }

    public Request getType() {
        return type;
    }

    public void setType(Request type) {
        this.type = type;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "RequestModel{" +
                "type=" + type +
                ", data='" + data + '\'' +
                '}';
    }
}

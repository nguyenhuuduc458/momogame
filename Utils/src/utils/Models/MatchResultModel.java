/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Models;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author nguyen huu duc
 */
public class MatchResultModel implements Comparable<MatchResultModel> {

    private String id;
    private String member_id;
    private String match_id;
    private Date date;

    private int total_point;
    private boolean result;

    public MatchResultModel() {

    }

    public MatchResultModel(String id, String member_id, String match_id, String date, int total_point, boolean result) {
        this.id = id;
        this.member_id = member_id;
        this.match_id = match_id;
        setDate(date);
        this.total_point = total_point;
        this.result = result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getMatch_id() {
        return match_id;
    }

    public void setMatch_id(String match_id) {
        this.match_id = match_id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(String input) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd HH:mm:ss");
        try {
            Date date = sdf.parse(input);
            this.date = date;
        } catch (ParseException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public int getTotal_point() {
        return total_point;
    }

    public void setTotal_point(int total_point) {
        this.total_point = total_point;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    @Override
    public int compareTo(MatchResultModel o) {
        return getDate().compareTo(o.getDate());
    }

}

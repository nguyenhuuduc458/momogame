/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Models;

import java.util.List;
import java.util.regex.MatchResult;
import utils.DTO.MemberDTO;

/**
 *
 * @author nguyen huu duc
 */
public class UserDetailModel {
    private MemberDTO member;
    private int totalMatch;
    private String winRate;
    private String lossRate;
    private String maxChainOfWin;
    private String maxChainOfLoss;
    private List<MatchResultModel> matchResult;

    public UserDetailModel() {
    }

    public MemberDTO getMember() {
        return member;
    }

    public void setMember(MemberDTO member) {
        this.member = member;
    }

    public int getTotalMatch() {
        return totalMatch;
    }

    public void setTotalMatch(int totalMatch) {
        this.totalMatch = totalMatch;
    }

    public String getWinRate() {
        return winRate;
    }

    public void setWinRate(String winRate) {
        this.winRate = winRate;
    }

    public String getLossRate() {
        return lossRate;
    }

    public void setLossRate(String lossRate) {
        this.lossRate = lossRate;
    }

    public List<MatchResultModel> getMatchResult() {
        return matchResult;
    }

    public void setMatchResult(List<MatchResultModel> matchResult) {
        this.matchResult = matchResult;
    }

    public String getMaxChainOfWin() {
        return maxChainOfWin;
    }

    public void setMaxChainOfWin(String maxChainOfWin) {
        this.maxChainOfWin = maxChainOfWin;
    }

    public String getMaxChainOfLoss() {
        return maxChainOfLoss;
    }

    public void setMaxChainOfLoss(String maxChainOfLoss) {
        this.maxChainOfLoss = maxChainOfLoss;
    }
    
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Models;

import java.util.List;
import utils.DTO.AnswerDTO;
import utils.DTO.QuestionDTO;

/**
 *
 * @author nguyen huu duc
 */
public class QuestionModel {
    private QuestionDTO question;
    private List<AnswerDTO> answers;
    private int rightAnswerPos;

    public QuestionModel() {}
    public QuestionModel(QuestionDTO question, List<AnswerDTO> answers, int rightAnswerPos) {
        this.question = question;
        this.answers = answers;
        this.rightAnswerPos = rightAnswerPos;
    }

    public QuestionDTO getQuestion() {
        return question;
    }

    public void setQuestion(QuestionDTO question) {
        this.question = question;
    }

    public List<AnswerDTO> getAnswer() {
        return answers;
    }

    public void setAnswer(List<AnswerDTO> answers) {
        this.answers = answers;
    }

    public int getRightAnswerPos() {
        return rightAnswerPos;
    }

    public void setRightAnswerPos(int rightAnswerPos) {
        this.rightAnswerPos = rightAnswerPos;
    }
    
    public int findRighAnswer(){
        for (int i = 0 ; i< answers.size(); i++) {
            if (answers.get(i).isIsTrue()) {
                rightAnswerPos = i;
                break;
            }
        }
        return 0;
    }
    
}

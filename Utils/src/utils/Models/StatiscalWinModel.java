/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Models;

/**
 *
 * @author nguyen huu duc
 */
public class StatiscalWinModel extends StatiscalModel{

    private int numberOfWin;
    private int accumulate_point;
    
    public StatiscalWinModel(String firstname, String lastname,int accumulate_point, int numberOfWin) {
        super(firstname, lastname);
        this.accumulate_point = accumulate_point;
        this.numberOfWin = numberOfWin;
    }

    public int getAccumulate_point() {
        return accumulate_point;
    }

    public void setAccumulate_point(int accumulate_point) {
        this.accumulate_point = accumulate_point;
    }

    public int getNumberOfWin() {
        return numberOfWin;
    }

    public void setNumberOfWin(int numberOfWin) {
        this.numberOfWin = numberOfWin;
    }
    
    
}

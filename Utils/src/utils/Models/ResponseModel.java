package utils.Models;

import utils.Interfaces.Request;

public class ResponseModel {
    private Request type;
    private boolean status;
    private String data;

    public ResponseModel() {
    }

    public ResponseModel(Request type, boolean status, String data) {
        this.type = type;
        this.status = status;
        this.data = data;
    }

    public Request getType() {
        return type;
    }

    public void setType(Request type) {
        this.type = type;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "ResponseModel{" +
                "type=" + type +
                ", status=" + status +
                ", data='" + data + '\'' +
                '}';
    }
}

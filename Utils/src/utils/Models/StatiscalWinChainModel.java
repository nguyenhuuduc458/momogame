/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Models;

/**
 *
 * @author nguyen huu duc
 */
public class StatiscalWinChainModel extends StatiscalModel {

    private int winChain;

    public StatiscalWinChainModel() {

    }

    public StatiscalWinChainModel(String firstname, String lastname, int winChain) {
        super(firstname, lastname);
        this.winChain = winChain;
    }

    public int getWinChain() {
        return winChain;
    }

    public void setWinChain(int winChain) {
        this.winChain = winChain;
    }

}

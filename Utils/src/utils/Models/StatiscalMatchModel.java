/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Models;

/**
 *
 * @author nguyen huu duc
 */
public class StatiscalMatchModel extends StatiscalModel {

    private int matchnumber;
    public StatiscalMatchModel(String firstname, String lastname, int matchnumber) {
        super(firstname, lastname);
        this.matchnumber = matchnumber;
    }

    public int getMatchnumber() {
        return matchnumber;
    }

    public void setMatchnumber(int matchnumber) {
        this.matchnumber = matchnumber;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.DTO;

import java.util.Date;
import utils.Models.MatchResultModel;

/**
 *
 * @author nguyen huu duc
 */
public class MatchResultDTO {

    private String id;
    private String member_id;
    private String match_id;
    private String date;

    private int total_point;
    private boolean result;

    public MatchResultDTO() {
    }

    public MatchResultDTO(String id, String member_id, String match_id, int total_point, boolean result) {
        this.id = id;
        this.member_id = member_id;
        this.match_id = match_id;
        this.total_point = total_point;
        this.result = result;
    }

    public MatchResultDTO(String id, String member_id, String match_id, String date) {
        this.id = id;
        this.member_id = member_id;
        this.match_id = match_id;
        this.date = date;

    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMember_id() {
        return member_id;
    }

    public void setMember_id(String member_id) {
        this.member_id = member_id;
    }

    public String getMatch_id() {
        return match_id;
    }

    public void setMatch_id(String match_id) {
        this.match_id = match_id;
    }

    public int getTotal_point() {
        return total_point;
    }

    public void setTotal_point(int total_point) {
        this.total_point = total_point;
    }

    public boolean isResult() {
        return result;
    }

    public void setResult(boolean result) {
        this.result = result;
    }

    public MatchResultModel mappingToModel() {
        MatchResultModel model = new MatchResultModel();
        model.setId(id);
        model.setDate(date);
        model.setMember_id(member_id);
        model.setMatch_id(match_id);
        model.setResult(result);
        model.setTotal_point(total_point);
        return model;
    }
}

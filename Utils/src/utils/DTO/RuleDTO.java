/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.DTO;

/**
 *
 * @author nguyen huu duc
 */
public class RuleDTO {
   private String id;
   private String content;
   private int value;

    public RuleDTO() {
    }

    public RuleDTO(String id, String content, int value) {
        this.id = id;
        this.content = content;
        this.value = value;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
   
}

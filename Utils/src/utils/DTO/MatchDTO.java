package utils.DTO;

public class MatchDTO {
    private String id;
    private String match_name;

    public MatchDTO() {
    }

    public MatchDTO(String id, String match_name) {
        this.id = id;
        this.match_name = match_name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMatch_name() {
        return match_name;
    }

    public void setMatch_name(String match_name) {
        this.match_name = match_name;
    }
    
}

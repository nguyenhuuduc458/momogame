package utils.DTO;

import java.util.Date;

public class MemberDTO {
    private String id;
    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private String sex; 
    private String birthday;
    private int iq_point = 0;
    private boolean is_active = true;
    private int accumulate_point = 0;

    public MemberDTO() {
    }

    public MemberDTO(String id, String username, String firstname, String lastname, String sex, String birthday, String password) {
        this.id = id;
        this.username = username;
        this.firstname = firstname;
        this.lastname = lastname;
        this.sex = sex;
        this.birthday = birthday;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public int getIQPoint() {
        return iq_point;
    }

    public void setIQpoint(int iq_point) {
        this.iq_point = iq_point;
    }

    public int getAccumulatePoint() {
        return accumulate_point;
    }

    public void setAccumulatePoint(int accumulate_point) {
        this.accumulate_point = accumulate_point;
    }
   
    public boolean isActive() {
        return is_active;
    }

    public void setActive(boolean is_active) {
        this.is_active = is_active;
    }

    public String getFullName() {
        return this.firstname + " " + this.lastname;
    }

}

package utils.DTO;

public class AnswerDTO {
    private String id;
    private String content;
    private boolean is_true;
    private String question_id;

    public AnswerDTO() {
    }

    public AnswerDTO(String id, String content, boolean isTrue, String questionId) {
        this.id = id;
        this.content = content;
        this.is_true = isTrue;
        this.question_id = questionId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public boolean isIsTrue() {
        return is_true;
    }

    public void setIsTrue(boolean isTrue) {
        this.is_true = isTrue;
    }

    public String getQuestionId() {
        return question_id;
    }

    public void setQuestionId(String questionId) {
        this.question_id = questionId;
    }
    
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Common;

import com.fatboyindustrial.gsonjodatime.Converters;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.net.ntp.NTPUDPClient;
import org.apache.commons.net.ntp.NtpV3Packet;
import org.apache.commons.net.ntp.TimeInfo;
import org.joda.time.DateTime;
import utils.Interfaces.Job;

/**
 *
 * @author nguyen huu duc
 */
public class CountDownTimer {

    private volatile boolean pause = false;
    private volatile boolean stop = false;

    public void setStop(boolean stop) {
        this.stop = stop;
    }

    public CountDownTimer() {

    }

    public DateTime getAutomicTime() {
        try {
            String TIME_SERVER = "1.vn.pool.ntp.org";
            NTPUDPClient timeClient = new NTPUDPClient();
            InetAddress inetAddress = InetAddress.getByName(TIME_SERVER);
            TimeInfo timeInfo = timeClient.getTime(inetAddress);
            NtpV3Packet message = timeInfo.getMessage();
            long serverTime = message.getTransmitTimeStamp().getTime();
            DateTime time = new DateTime(serverTime);
            return time;
        } catch (UnknownHostException ex) {
            System.out.println("Không kết nối được với NTP server");
        } catch (IOException ex) {
            System.out.println("Error when transfer data" + ex.getLocalizedMessage());
        }
        return null;
    }

    public DateTime setStartCountDownAfter(int millisecond) {
        DateTime start = getAutomicTime().plusMillis(millisecond);
        return start;
    }

    public void startWithAction(final int second, Job job) {
        ExecutorService executor = Executors.newFixedThreadPool(10);
        Future<Integer> result = executor.submit(() -> {
            int s = second;
            while (s >= 0) {
                s--;
                System.out.println(s);
                Thread.sleep(1000);
                boolean isRunning = job.doDuringCountdown();
                if (!isRunning) {
                    break;
                }
                if (s == 0) {
                    job.doAfterCountdown();
                    break;
                }
            }
            return 1;
        });
        try {
            int r = result.get();
        } catch (InterruptedException ex) {
            Logger.getLogger(CountDownTimer.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ExecutionException ex) {
            Logger.getLogger(CountDownTimer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void startWithActionForClient(final int second, Job job) {
        ExecutorService executor = Executors.newCachedThreadPool();
        Future<Integer> result = executor.submit(() -> {
            int s = second;
            while (s >= 0) {
                s--;
                Thread.sleep(1000);
                if (stop) {
                    System.out.println("Stop countdown");
                    break;
                }
                
                boolean isRunning = job.doDuringCountdown();
                if (!isRunning) {
                    break;
                }
                synchronized (this) {
                    if (pause) {
                        System.out.println("Pause countdown ");
                        wait();
                    }
                }
                if (s == 0) {
                    System.out.println("Stop countdown");
                    job.doAfterCountdown();
                    break;
                }
            }
            return 1;
        });
        try {
            int r = result.get();
        } catch (InterruptedException ex) {
            System.out.println("Error when countdown timer");
        } catch (ExecutionException ex) {
            System.out.println("Error when countdown timer");
        }
    }

    public Gson getGsonSerializeDateTime() {
        return Converters.registerDateTime(new GsonBuilder()).create();
    }

    public void setPause(boolean pause) {
        new Thread(() -> {
            if (pause) {
                this.pause = pause;
            } else {
                this.pause = pause;
                synchronized (this) {
                    notifyAll();
                }
            }
        }).start();
    }
  
}

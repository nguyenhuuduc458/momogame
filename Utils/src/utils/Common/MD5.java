package utils.Common;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class MD5 {
    public MD5() {
    }

    public String getHashString(String plaintext) {
        try {
            MessageDigest md = MessageDigest.getInstance("md5");
            byte[] messageDigest = md.digest(plaintext.getBytes());
            BigInteger no = new BigInteger(1, messageDigest);
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } catch (NoSuchAlgorithmException e) {
            System.out.println("Error when hashing data: " + e.getMessage());
        }
        return null;
    }
}

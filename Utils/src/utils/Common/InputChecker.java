/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Common;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author mvhix
 */
public class InputChecker {

    // regex patterns
    private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private static final String ALPHANUMERIC_PATTERN = "^[A-Za-z0-9]+$";

    private static final String WORD_PATTERN = "[\\pL\\s]+";

    private static final String HOSTNAME_PATTERN = "^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9\\-]*[A-Za-z0-9])$";

    private static final String PORTNUMBER_PATTERN = "^([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])$";

    private static final String SQL_PATTER = "('(''|[^'])*')|(;)|(\\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE){0,1}|INSERT( +INTO){0,1}|MERGE|SELECT|UPDATE|UNION( +ALL){0,1})\\b\')";

    // variables
    private static Matcher matcher;
    private static Pattern pattern;

    // check empty
    public static boolean isEmpty(String str) {
        if (str.equals("")) {
            return true;
        }
        return false;
    }

    // check if a string passed in is a word (including utf-8 type, spaces)
    public static boolean isWord(String str) {

        pattern = Pattern.compile(WORD_PATTERN);
        matcher = pattern.matcher(str);

        return matcher.matches();
    }

    // check if a string passed in is a number in double type
    public static boolean isNumber(String str) {
        try {
            double number = Double.parseDouble(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isStringContainNumber(String str) {
        char[] chars = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (char c : chars) {
            if (Character.isDigit(c)) {
                return true;
            }
        }
        return false;
    }

    // check if a string passed in contains letters and numbers

    public static boolean isAlphanumeric(String str) {

        pattern = Pattern.compile(ALPHANUMERIC_PATTERN);
        matcher = pattern.matcher(str);

        return matcher.matches();
    }

    public static boolean isEmail(String str) {

        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(str);

        return matcher.matches();
    }

    public static boolean isHostName(String str) {

        pattern = Pattern.compile(HOSTNAME_PATTERN);
        matcher = pattern.matcher(str);

        return matcher.matches();
    }

    public static boolean isPortNumber(String str) {

        pattern = Pattern.compile(PORTNUMBER_PATTERN);
        matcher = pattern.matcher(str);

        return matcher.matches();
    }

    public static boolean isContainSQL(String str) {
        String s = str.toUpperCase();
        pattern = Pattern.compile(SQL_PATTER);
        matcher = pattern.matcher(s);
        return matcher.find();
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Common;

import java.awt.Image;
import javax.swing.ImageIcon;

/**
 *
 * @author nguyen huu duc
 */
public class ImageHelper {

    public ImageHelper() {

    }

    public ImageIcon scaleImage(ImageIcon icon, int w, int h) {
        try {
            if(icon == null) return null;
            int nw = icon.getIconWidth();
            int nh = icon.getIconHeight();

            if (icon.getIconWidth() > w) {
                nw = w;
                nh = (nw * icon.getIconHeight()) / icon.getIconWidth();
            }

            if (nh > h) {
                nh = h;
                nw = (icon.getIconWidth() * nh) / icon.getIconHeight();
            }

            return new ImageIcon(icon.getImage().getScaledInstance(nw, nh, Image.SCALE_DEFAULT));
        } catch (NullPointerException e) {
            return null;
        }
    }
}

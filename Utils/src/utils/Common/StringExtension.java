/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Common;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import jdk.nashorn.internal.runtime.regexp.joni.Regex;

import org.apache.commons.lang3.StringEscapeUtils;

/**
 * @author nguyen huu duc
 */
public class StringExtension {

    // prevent sql injection attack
    public static String escapeSQL(String statement) {
        return StringEscapeUtils.escapeJava(statement);
    }

    public static char[] generateOTP(int length) {
        Random obj = new Random();
        char[] otp = new char[length];
        for (int i = 0; i < length; i++) {
            otp[i] = (char) (obj.nextInt(10) + 48);
        }
        return otp;
    }

     public static String addSlashes(String s) {
        s = s.replaceAll("\\\\", "\\\\\\\\");
        s = s.replaceAll("\\n", "\\\\n");
        s = s.replaceAll("\\r", "\\\\r");
        s = s.replaceAll("\\00", "\\\\0");
        s = s.replaceAll("'", "\\\\'");
        return s;
    }
     
    public static String getOTPTemplate(String username, char[] otp) {
        return "<table align=\"center\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\"bgcolor=\"#f0f0f0\">\n"
                + "        <tr>\n"
                + "        <td style=\"padding: 30px 30px 20px 30px;\">\n"
                + "            <table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" width=\"100%\" bgcolor=\"#ffffff\" style=\"max-width: 650px; margin: auto;\">\n"
                + "            <tr>\n"
                + "                <td colspan=\"2\" align=\"center\" style=\"background-color: #333; padding: 40px;\">\n"
                + "                    <a href=\"http://wso2.com/\" target=\"_blank\"><img src=\"http://cdn.wso2.com/wso2/newsletter/images/nl-2017/wso2-logo-transparent.png\" border=\"0\" /></a>\n"
                + "                </td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "                <td colspan=\"2\" align=\"center\" style=\"padding: 50px 50px 0px 50px;\">\n"
                + "                    <h1 style=\"padding-right: 0em; margin: 0; line-height: 40px; font-weight:300; font-family: 'Nunito Sans', Arial, Verdana, Helvetica, sans-serif; color: #666; text-align: left; padding-bottom: 1em;\">\n"
                + "                        Verify your account\n"
                + "                    </h1>\n"
                + "                </td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "                <td style=\"text-align: left; padding: 0px 50px;\" valign=\"top\">\n"
                + "                    <p style=\"font-size: 18px; margin: 0; line-height: 24px; font-family: 'Nunito Sans', Arial, Verdana, Helvetica, sans-serif; color: #666; text-align: left; padding-bottom: 3%;\">\n"
                + "                        Hi " + username + ",\n"
                + "                    </p>\n"
                + "                    <p style=\"font-size: 18px; margin: 0; line-height: 24px; font-family: 'Nunito Sans', Arial, Verdana, Helvetica, sans-serif; color: #666; text-align: left; padding-bottom: 3%;\">\n"
                + "                        Please use this one time password <b>" + String.valueOf(otp) + "</b> to register to your account\n"
                + "                    </p>\n"
                + "                </td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "                <td style=\"text-align: left; padding: 30px 50px 50px 50px\" valign=\"top\">\n"
                + "                    <p style=\"font-size: 18px; margin: 0; line-height: 24px; font-family: 'Nunito Sans', Arial, Verdana, Helvetica, sans-serif; color: #505050; text-align: left;\">\n"
                + "                        Thanks,\n"
                + "                    </p>\n"
                + "                </td>\n"
                + "            </tr>\n"
                + "            <tr>\n"
                + "                <td colspan=\"2\" align=\"center\" style=\"padding: 20px 40px 40px 40px;\" bgcolor=\"#f0f0f0\">\n"
                + "                    <p style=\"font-size: 12px; margin: 0; line-height: 24px; font-family: 'Nunito Sans', Arial, Verdana, Helvetica, sans-serif; color: #777;\">\n"
                + "                        &copy; 2018\n"
                + "                        <a href=\"http://wso2.com/\" target=\"_blank\" style=\"color: #777; text-decoration: none\">WSO2</a>\n"
                + "                        <br>\n"
                + "                        787 Castro Street, Mountain View, CA 94041.\n"
                + "                    </p>\n"
                + "                </td>\n"
                + "            </tr>\n"
                + "            </table>\n"
                + "        </td>\n"
                + "    </tr>\n"
                + "</table>";
    }

    public static void main(String[] args) {
        String input = "SELECT";
        String tam = "nguyen nam's";
        String regex = "('(''|[^'])*')|(;)|(\\b(ALTER|CREATE|DELETE|DROP|EXEC(UTE){0,1}|INSERT( +INTO){0,1}|MERGE|SELECT|UPDATE|UNION( +ALL){0,1})\\b)";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        if(matcher.matches()){
            System.out.println("khong hop le");
        }
        System.out.println(tam);
    }
}

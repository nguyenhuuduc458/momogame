/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils.Common;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;
import javax.activation.*;

/**
 * @author nguyen huu duc
 */
public class Email extends Thread {

    private String content;
    private String text;
    private String contentType = "text/html";
    private String subject;
    private String sender;
    private String recipient;
    private String passwordSender;

    public Email() {
    }

    private Email(Builder builder) {
        this.sender = builder.sender;
        this.recipient = builder.recipient;
        this.passwordSender = builder.passwordSender;
        this.content = builder.content;
        this.contentType = builder.contentType;
        this.text = builder.text;
        this.subject = builder.subject;
    }

    public static class Builder {

        private String content;
        private String contentType = "text/html";
        private String text;
        private String subject;
        private String sender;
        private String recipient;
        private String passwordSender;

        public Builder(String sender, String recipient, String passwordSender) {
            this.sender = sender;
            this.recipient = recipient;
            this.passwordSender = passwordSender;
        }

        public Builder setText(String text) {
            this.text = text;
            return this;
        }

        public Builder setSubject(String subject) {
            this.subject = subject;
            return this;
        }

        public Builder setContent(String content) {
            this.content = content;
            return this;
        }

        public Builder setContentType(String contentType) {
            this.contentType = contentType;
            return this;
        }

        public Email build() {
            return new Email(this);
        }

    }

    public boolean send() {
        //Get properties object
        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");

        //get Session
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(sender, passwordSender);
                    }
                });

        //compose message
        try {
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(sender));
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));
            message.setSubject(subject != null ? subject : "");
            message.setText(text != null ? text : "");
            message.setContent(content != null ? content : null, contentType);

            //send message
            Transport.send(message);
            System.out.println("message sent successfully");
            return true;
        } catch (MessagingException e) {
            System.out.println("Error when sending Email: " + e.getMessage());
        }
        return false;
    }

    @Override
        public void run() {
        super.run();
        send();
    }

}

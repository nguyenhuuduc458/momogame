/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.GUI.waitingroom;

import client.GUI.iqtest.IQTestForm;
import client.GUI.main.LoginForm;
import client.GUI.ranklookup.RankLookUpForm;
import client.Socket.ClientManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.awt.Component;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.ConnectException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import org.joda.time.DateTime;
import utils.Common.CountDownTimer;
import utils.DTO.MemberDTO;
import utils.DTO.RuleDTO;
import utils.Encrypt.TripleDES;
import utils.Env.Const;
import utils.Interfaces.HandleMessage;
import utils.Interfaces.Request;
import utils.Models.RequestModel;
import utils.Models.ResponseModel;

/**
 *
 * @author mvhix
 */
public class WaitingRoomForm extends javax.swing.JFrame {

    public static volatile MemberDTO member;
    public static volatile BufferedReader receive;
    public static volatile BufferedWriter send;

    enum rule {

        TIME_ANSWER,
        QUESTION_NUMBER,
        POINT_PLUS,
    }

    /**
     * Creates new form Demo
     */
    public WaitingRoomForm() {
        initComponents();
        customInitComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        topPanel = new javax.swing.JPanel();
        lblForm = new javax.swing.JLabel();
        btnClose = new javax.swing.JButton();
        btnMinimize = new javax.swing.JButton();
        mainPanel = new javax.swing.JPanel();
        btnFindAMatch = new javax.swing.JButton();
        btnOptions = new javax.swing.JButton();
        lblUserFullName = new javax.swing.JLabel();
        btnRankLookUp = new javax.swing.JButton();
        btnTestIQ = new javax.swing.JButton();
        lblTitle = new javax.swing.JLabel();
        lblImageLoading = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Phòng chờ");
        setUndecorated(true);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        topPanel.setBackground(new java.awt.Color(255, 255, 255));
        topPanel.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                topPanelMouseDragged(evt);
            }
        });
        topPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                topPanelMousePressed(evt);
            }
        });

        lblForm.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblForm.setText("Phòng chờ");

        btnClose.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnClose.setText("X");
        btnClose.setToolTipText("Đóng");
        btnClose.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });

        btnMinimize.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnMinimize.setText("__");
        btnMinimize.setToolTipText("Thu nhỏ");
        btnMinimize.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnMinimize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMinimizeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout topPanelLayout = new javax.swing.GroupLayout(topPanel);
        topPanel.setLayout(topPanelLayout);
        topPanelLayout.setHorizontalGroup(
            topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(topPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblForm)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnMinimize)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnClose))
        );
        topPanelLayout.setVerticalGroup(
            topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, topPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(btnClose)
                    .addComponent(btnMinimize)
                    .addComponent(lblForm))
                .addContainerGap())
        );

        mainPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        btnFindAMatch.setBackground(new java.awt.Color(204, 204, 204));
        btnFindAMatch.setFont(new java.awt.Font("Segoe UI", 1, 22)); // NOI18N
        btnFindAMatch.setText("TÌM TRẬN");
        btnFindAMatch.setToolTipText("");
        btnFindAMatch.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnFindAMatch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFindAMatchActionPerformed(evt);
            }
        });

        btnOptions.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnOptions.setIcon(new javax.swing.ImageIcon(getClass().getResource("/client/GUI/img/down_button_20px.png"))); // NOI18N
        btnOptions.setToolTipText("Tùy chọn");
        btnOptions.setBorder(null);
        btnOptions.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnOptions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOptionsActionPerformed(evt);
            }
        });

        lblUserFullName.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblUserFullName.setIcon(new javax.swing.ImageIcon(getClass().getResource("/client/GUI/img/user_20px.png"))); // NOI18N
        lblUserFullName.setText("userfullname");

        btnRankLookUp.setBackground(new java.awt.Color(204, 204, 204));
        btnRankLookUp.setFont(new java.awt.Font("Segoe UI", 1, 22)); // NOI18N
        btnRankLookUp.setText("TRA CỨU XẾP HẠNG");
        btnRankLookUp.setToolTipText("");
        btnRankLookUp.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRankLookUp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRankLookUpActionPerformed(evt);
            }
        });

        btnTestIQ.setBackground(new java.awt.Color(204, 204, 204));
        btnTestIQ.setFont(new java.awt.Font("Segoe UI", 1, 22)); // NOI18N
        btnTestIQ.setText("KIỂM TRA IQ");
        btnTestIQ.setToolTipText("");
        btnTestIQ.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnTestIQ.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTestIQActionPerformed(evt);
            }
        });

        lblTitle.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        lblTitle.setText("TITLE");

        lblImageLoading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/client/GUI/img/ajax-loader (1).gif"))); // NOI18N
        lblImageLoading.setToolTipText("");

        javax.swing.GroupLayout mainPanelLayout = new javax.swing.GroupLayout(mainPanel);
        mainPanel.setLayout(mainPanelLayout);
        mainPanelLayout.setHorizontalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                .addContainerGap(165, Short.MAX_VALUE)
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                        .addComponent(lblUserFullName)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnOptions)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                        .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(btnFindAMatch, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnRankLookUp, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnTestIQ, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblImageLoading))
                        .addGap(146, 146, 146))))
        );
        mainPanelLayout.setVerticalGroup(
            mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, mainPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(mainPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btnOptions, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblUserFullName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(30, 30, 30)
                .addComponent(lblTitle)
                .addGap(34, 34, 34)
                .addComponent(btnFindAMatch, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50)
                .addComponent(btnRankLookUp, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(50, 50, 50)
                .addComponent(btnTestIQ, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(lblImageLoading)
                .addContainerGap(36, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(topPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(topPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void customInitComponents() {
        this.setLocationRelativeTo(null);   // set the form in the screen center

        btnMinimize.setBorderPainted(false);
        btnMinimize.setFocusPainted(false);
        btnMinimize.setContentAreaFilled(false);

        btnClose.setBorderPainted(false);
        btnClose.setFocusPainted(false);
        btnClose.setContentAreaFilled(false);

        btnFindAMatch.setBorderPainted(false);
        btnFindAMatch.setFocusPainted(false);

        btnOptions.setOpaque(false);
        btnOptions.setContentAreaFilled(false);
        btnOptions.setBorderPainted(false);
        btnOptions.setFocusPainted(false);

        btnRankLookUp.setBorderPainted(false);
        btnRankLookUp.setFocusPainted(false);

        btnTestIQ.setBorderPainted(false);
        btnTestIQ.setFocusPainted(false);

        lblTitle.setText("<html><p style=\"text-align: center;\">CHÀO MỪNG BẠN ĐẾN VỚI <br/>"
                + "<i>\"TRANH TÀI KIẾN THỨC\"</i></p></html>");
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);

        // a popup menu shows options
        menu = new JPopupMenu();
        optUpdateProfile = new JMenuItem("Thay đổi thông tin");
        optLogout = new JMenuItem("Đăng xuất");

        // add menu item into popup menu
        menu.add(optUpdateProfile);
        menu.add(optLogout);

        // add event listener to menu items
        optUpdateProfile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                optUpdateProfileActionPerformed(evt);
            }
        });

        optLogout.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                optLogoutActionPerformed(evt);
            }
        });

        //loading icon 
        lblImageLoading.setVisible(false);

        isSwitchForm = false;

        //check inte
        checkInternetAndConnect();

    }

    private void btnMinimizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMinimizeActionPerformed
        this.setState(ICONIFIED);
    }//GEN-LAST:event_btnMinimizeActionPerformed


    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        // custom option buttons
        Object[] options = {"Thoát", "Không"};

        // get choice from user
        int choice = JOptionPane.showOptionDialog(null, "Bạn có chắc muốn thoát hay không ?", "Thông báo",
                JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[0]);

        if (choice == JOptionPane.OK_OPTION) {
            service.shutdownNow();
            this.dispose();
            try {
                RequestModel requestModel = new RequestModel(Request.LOGOUT, null);
                sendMessage(WaitingRoomForm.send, new Gson().toJson(requestModel));
            } catch (UnknownHostException e) {
                System.out.println("Host không hợp lệ");
                JOptionPane.showMessageDialog(null, "Không tồn tại địa chỉ host để kết nối đến server");
            } catch (ConnectException e) {

            } catch (IOException e) {
                System.out.println(e.getMessage());
            } catch (NullPointerException e) {
                // do nothing 
            }
            ClientManager.getInstance().closeConnection();
            new LoginForm().setVisible(true);

        }
    }//GEN-LAST:event_btnCloseActionPerformed

    private void btnFindAMatchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFindAMatchActionPerformed
        // start loading icon
        isSwitchForm = true;
        lblImageLoading.setVisible(true);
        doInBackground(this.send, this.receive);
    }//GEN-LAST:event_btnFindAMatchActionPerformed

    public void sendMessage(BufferedWriter send, String message) throws IOException {
        String encryptData = new TripleDES().encrypt(message);
        send.write(encryptData);
        send.newLine();
        send.flush();

    }

    public void receiveMessage(BufferedReader receive) throws IOException {
        lblImageLoading.setVisible(false);
        switchForm();
    }

    public void doInBackground(BufferedWriter send, BufferedReader receive) {
        try {
            RequestModel rq = new RequestModel(Request.PAIRING, new Gson().toJson(member));
            sendMessage(send, new Gson().toJson(rq));
            receiveMessage(WaitingRoomForm.receive);
        } catch (UnknownHostException e) {
            System.out.println("Host không hợp lệ");
            JOptionPane.showMessageDialog(null, "Không tồn tại địa chỉ host để kết nối đến server");
        } catch (ConnectException e) {
        } catch (IOException e) {
        } catch (NullPointerException e) {
            // do nothing 
        }
    }

    private void topPanelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_topPanelMousePressed
        mouseX = evt.getX();
        mouseY = evt.getY();
    }//GEN-LAST:event_topPanelMousePressed

    private void topPanelMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_topPanelMouseDragged
        this.setLocation(this.getX() + evt.getX() - mouseX, this.getY() + evt.getY() - mouseY);
    }//GEN-LAST:event_topPanelMouseDragged

    private void btnOptionsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnOptionsActionPerformed
        // get the event source from the button
        component = (Component) evt.getSource();

        // get the location of the pointed button on the screen
        point = component.getLocationOnScreen();

        // show popup menu
        menu.show(this, 0, 0);  // 0, 0 is the co-ordinate of the popup menu
        // this is relative to the screen

        // set the location of the popup menu
        menu.setLocation(point.x - 100, point.y + component.getHeight());
    }//GEN-LAST:event_btnOptionsActionPerformed

    private void btnRankLookUpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRankLookUpActionPerformed
        isSwitchForm = true;
        this.dispose();
        new RankLookUpForm().setVisible(true);
        service.shutdown();
    }//GEN-LAST:event_btnRankLookUpActionPerformed

    private void btnTestIQActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnTestIQActionPerformed
        // TODO add your handling code here:
        isSwitchForm = true;
        this.dispose();
        new IQTestForm().setVisible(true);
    }//GEN-LAST:event_btnTestIQActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        lblUserFullName.setText(member == null ? "Unknown user" : member.getFullName());

        // send request update rule
        service.execute(new Runnable() {
            @Override
            public void run() {
                try {
                    requestUpdateRule();
                    responseUpdateRule();
                } catch (IOException ex) {
                } catch (NullPointerException e) {

                }
            }
        });
        service.shutdown();
    }//GEN-LAST:event_formWindowOpened

    public void requestUpdateRule() throws IOException {
        RequestModel request = new RequestModel(Request.UPDATE_RULE, null);
        String encryptData = new TripleDES().encrypt(new Gson().toJson(request));
        send.write(encryptData);
        send.newLine();
        send.flush();

    }

    public void responseUpdateRule() throws IOException {
        String response = new TripleDES().decrypt(WaitingRoomForm.receive.readLine());
        ResponseModel responseModel = new Gson().fromJson(response, ResponseModel.class);
        if (responseModel.getType().equals(Request.UPDATE_RULE)) {
            //update rule in client
            updateRule(new Gson().fromJson(responseModel.getData(), new TypeToken<ConcurrentHashMap<String, RuleDTO>>() {
            }.getType()));
            System.out.println("Update rule thành công");
        }

    }

    public void updateRule(ConcurrentHashMap<String, RuleDTO> rules) {
        rules.keySet().stream().forEach((key) -> {
            if (rules.get(key).getContent().equals(rule.QUESTION_NUMBER.toString())) {
                Const.QUESTION_NUMBER = rules.get(key).getValue();
            } else if (rules.get(key).getContent().equals(rule.POINT_PLUS.toString())) {
                Const.POINT_PLUS = rules.get(key).getValue();
            } else if (rules.get(key).getContent().equals(rule.TIME_ANSWER.toString())) {
                Const.TIME_ANSWER = rules.get(key).getValue();
            }
        });
    }

    private void optUpdateProfileActionPerformed(java.awt.event.ActionEvent evt) {
        isSwitchForm = true;
        this.dispose();
        new UpdateUserInfoForm().setVisible(true);
    }

    private void optLogoutActionPerformed(java.awt.event.ActionEvent evt) {
        isSwitchForm = true;
        this.dispose();
        service.shutdown();
        try {
            RequestModel requestModel = new RequestModel(Request.LOGOUT, null);
            sendMessage(WaitingRoomForm.send, new Gson().toJson(requestModel));
        } catch (IOException ex) {
        }
        ClientManager.getInstance().closeConnection();
        new LoginForm().setVisible(true);
    }

    private void showLoading() {
        lblImageLoading.setVisible(true);
    }

    private void exitForm() {
        this.dispose();
    }

    // handle logic
    public void setArguments(MemberDTO member, BufferedReader receive, BufferedWriter send) {
        this.member = member;
        lblUserFullName.setText(this.member == null ? "Unknown user" : member.getFullName());
        this.receive = receive;
        this.send = send;
    }

    private void saveStream(BufferedReader receive, BufferedWriter send) {
        this.receive = receive;
        this.send = send;
    }

    public void switchForm() {
        isSwitchForm = true;
        service.shutdown();
        exitForm();
        new ParingForm().setVisible(true);
    }

    private void checkInternetAndConnect() {
        System.out.println("Open check internet *WAITING FORM* ");
        new Thread(() -> {
            while (!isSwitchForm) {
                if (isSwitchForm) {
                    System.out.println("Close check internet *WAITING FORM* ");
                    break;
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(WaitingRoomForm.class.getName()).log(Level.SEVERE, null, ex);
                }
                if (isSwitchForm) {
                    System.out.println("Close check internet *WAITING FORM* ");
                    break;
                }
                if (!CheckInternet()) {
                    isSwitchForm = true;
                    handleLostConnect();
                    break;
                }
                try {
                    sendMessage(WaitingRoomForm.send, new Gson().toJson(new RequestModel(Request.CHECK_CONNECTION, "send trong waiting room")));
                } catch (IOException ex) {
                    break;
                }

            }
            System.out.println("Ra khỏi check trong waiting room");
        }).start();
    }

    private boolean CheckInternet() {
        try {
            URL url = new URL("http://www.google.com");
            URLConnection connection = url.openConnection();
            connection.connect();
            return true;
        } catch (MalformedURLException e) {
            isSwitchForm = true;
            System.out.println("Internet is not connected");
            return false;
        } catch (IOException e) {
            isSwitchForm = true;
            System.out.println("Internet is not connected");
            return false;
        }
    }

    public void handleLostConnect() {
        try {
            ClientManager.getInstance().closeConnection();
            service.shutdown();
            JOptionPane.showMessageDialog(null, "Mất kết nối internet phiên đăng nhập đã hết hạn vui lòng đăng nhập lại");
            this.dispose();
            new LoginForm().setVisible(true);
        } catch (Exception e) {

        }
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new WaitingRoomForm().setVisible(true);
            }
        });
    }

    // custom variables
    private int mouseX;
    private int mouseY;
    private JPopupMenu menu;
    private JMenuItem optUpdateProfile, optLogout;
    private Component component;
    private Point point;
    private volatile boolean isSwitchForm = false;
    private volatile ExecutorService service = Executors.newFixedThreadPool(2);
    // end of custom variables


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnFindAMatch;
    private javax.swing.JButton btnMinimize;
    private javax.swing.JButton btnOptions;
    private javax.swing.JButton btnRankLookUp;
    private javax.swing.JButton btnTestIQ;
    private javax.swing.JLabel lblForm;
    private javax.swing.JLabel lblImageLoading;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JLabel lblUserFullName;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JPanel topPanel;
    // End of variables declaration//GEN-END:variables
}

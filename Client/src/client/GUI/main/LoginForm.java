/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.GUI.main;

import Server.BUS.RuleBUS;
import client.GUI.waitingroom.WaitingRoomForm;
import utils.DTO.MemberDTO;
import utils.Interfaces.HandleMessage;
import utils.Interfaces.Request;
import utils.Models.Account;
import utils.Models.RequestModel;
import utils.Models.ResponseModel;
import utils.Common.MD5;
import utils.Encrypt.TripleDES;
import utils.Env.Const;
import client.Socket.ClientManager;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.ConcurrentHashMap;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import utils.Common.InputChecker;
import utils.DTO.RuleDTO;

/**
 *
 * @author mvhix
 */
public class LoginForm extends javax.swing.JFrame {

    private Scanner scanner = new Scanner(System.in);
    private BufferedReader receive;
    private BufferedWriter send;

    /**
     * Creates new form Demo
     */
    public LoginForm() {
        initComponents();
        customInitComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        topPanel = new javax.swing.JPanel();
        lblForm = new javax.swing.JLabel();
        btnClose = new javax.swing.JButton();
        btnMinimize = new javax.swing.JButton();
        mainPanel = new javax.swing.JPanel();
        lblUsername = new javax.swing.JLabel();
        lblTitle = new javax.swing.JLabel();
        txtUsername = new javax.swing.JTextField();
        lblPassword = new javax.swing.JLabel();
        btnLogin = new javax.swing.JButton();
        txtPassword = new javax.swing.JPasswordField();
        lblCreateAccount = new javax.swing.JLabel();
        btnSetting = new javax.swing.JLabel();
        lblImageLoading = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Đăng nhập");
        setUndecorated(true);

        topPanel.setBackground(new java.awt.Color(255, 255, 255));
        topPanel.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                topPanelMouseDragged(evt);
            }
        });
        topPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                topPanelMousePressed(evt);
            }
        });

        lblForm.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        lblForm.setText("Đăng nhập");

        btnClose.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnClose.setText("X");
        btnClose.setToolTipText("Đóng");
        btnClose.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnClose.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCloseActionPerformed(evt);
            }
        });

        btnMinimize.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnMinimize.setText("__");
        btnMinimize.setToolTipText("Thu nhỏ");
        btnMinimize.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnMinimize.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMinimizeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout topPanelLayout = new javax.swing.GroupLayout(topPanel);
        topPanel.setLayout(topPanelLayout);
        topPanelLayout.setHorizontalGroup(
            topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(topPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lblForm)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnMinimize)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnClose))
        );
        topPanelLayout.setVerticalGroup(
            topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, topPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(topPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(btnClose)
                    .addComponent(btnMinimize)
                    .addComponent(lblForm))
                .addContainerGap())
        );

        mainPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        mainPanel.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblUsername.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        lblUsername.setText("Email");
        mainPanel.add(lblUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(142, 75, -1, -1));

        lblTitle.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        lblTitle.setText("THÔNG TIN ĐĂNG NHẬP");
        mainPanel.add(lblTitle, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 37, 473, -1));

        txtUsername.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtUsername.setForeground(new java.awt.Color(204, 204, 204));
        txtUsername.setText("Nhập tên email");
        txtUsername.setBorder(javax.swing.BorderFactory.createEmptyBorder(1, 1, 1, 1));
        txtUsername.setName(""); // NOI18N
        txtUsername.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtUsernameFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtUsernameFocusLost(evt);
            }
        });
        mainPanel.add(txtUsername, new org.netbeans.lib.awtextra.AbsoluteConstraints(142, 103, 200, 35));

        lblPassword.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        lblPassword.setText("Mật khẩu");
        mainPanel.add(lblPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(142, 144, -1, -1));

        btnLogin.setBackground(new java.awt.Color(204, 204, 204));
        btnLogin.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        btnLogin.setText("Đăng nhập");
        btnLogin.setToolTipText("");
        btnLogin.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnLogin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLoginActionPerformed(evt);
            }
        });
        mainPanel.add(btnLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 218, -1, -1));

        txtPassword.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txtPassword.setForeground(new java.awt.Color(204, 204, 204));
        txtPassword.setText("jPasswordField1");
        txtPassword.setBorder(null);
        txtPassword.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtPasswordFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtPasswordFocusLost(evt);
            }
        });
        txtPassword.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPasswordKeyPressed(evt);
            }
        });
        mainPanel.add(txtPassword, new org.netbeans.lib.awtextra.AbsoluteConstraints(142, 172, 200, 35));

        lblCreateAccount.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        lblCreateAccount.setText("Nhấn vào đây để đăng ký ngay");
        lblCreateAccount.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        lblCreateAccount.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lblCreateAccountMouseClicked(evt);
            }
        });
        mainPanel.add(lblCreateAccount, new org.netbeans.lib.awtextra.AbsoluteConstraints(132, 257, -1, -1));

        btnSetting.setIcon(new javax.swing.ImageIcon(getClass().getResource("/client/GUI/img/settings_icon_16.png"))); // NOI18N
        btnSetting.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnSettingMouseClicked(evt);
            }
        });
        mainPanel.add(btnSetting, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 260, -1, -1));

        lblImageLoading.setIcon(new javax.swing.ImageIcon(getClass().getResource("/client/GUI/img/ajax-loader (1).gif"))); // NOI18N
        mainPanel.add(lblImageLoading, new org.netbeans.lib.awtextra.AbsoluteConstraints(319, 219, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(topPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(topPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
                .addGap(50, 50, 50)
                .addComponent(mainPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void customInitComponents() {
        this.setLocationRelativeTo(null);   // set the form in the screen center

        btnMinimize.setBorderPainted(false);
        btnMinimize.setFocusPainted(false);
        btnMinimize.setContentAreaFilled(false);

        btnClose.setBorderPainted(false);
        btnClose.setFocusPainted(false);
        btnClose.setContentAreaFilled(false);

        btnLogin.setBorderPainted(false);
        btnLogin.setFocusPainted(false);

        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);

        // password field
        passwordChar = txtPassword.getEchoChar();
        txtPassword.setEchoChar((char) 0);
        txtPassword.setText("Nhập mật khẩu");

        // test data
//        txtUsername.setText("nguyenhuuduc225@gmail.com");
//        txtPassword.setText("123456");
        //disable loading
        lblImageLoading.setVisible(false);
    }

    private void btnMinimizeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMinimizeActionPerformed
        this.setState(ICONIFIED);
    }//GEN-LAST:event_btnMinimizeActionPerformed

    private void btnCloseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCloseActionPerformed
        System.exit(0);
        ClientManager.getInstance().closeConnection();
    }//GEN-LAST:event_btnCloseActionPerformed

    private void btnLoginActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLoginActionPerformed
        btnLogin.setEnabled(false);

        // mapping inputs
        username = txtUsername.getText();
        password = String.valueOf(txtPassword.getPassword());

        // start loading image
        lblImageLoading.setVisible(true);
        if (validateInput()) {
            new Thread(() -> {
                ClientManager.getInstance().runClient(new HandleMessage() {
                    @Override
                    public void sendMessage(BufferedWriter send, String message) throws IOException {
                        String encryptData = new TripleDES().encrypt(message);
                        send.write(encryptData);
                        send.newLine();
                        send.flush();
                    }

                    @Override
                    public void receiveMessage(BufferedReader receive) throws IOException {
                        String response = new TripleDES().decrypt(receive.readLine());
                        ResponseModel responseModel = new Gson().fromJson(response, ResponseModel.class);

                        if (responseModel.getType().equals(Request.LOGIN)) {
                            // stop loading 
                            lblImageLoading.setVisible(false);
                            if (responseModel.isStatus()) {
                                MemberDTO member = new Gson().fromJson(responseModel.getData(), MemberDTO.class);
                                WaitingRoomForm form = new WaitingRoomForm();
                                form.setArguments(member, getReceive(), getSend());
                                form.setVisible(true);
                                exitForm();

                            } else {
                                JOptionPane.showMessageDialog(null, responseModel.getData());
                                ClientManager.getInstance().closeConnection();
                            }

                        }
                    }

                    @Override
                    public void executeAction(BufferedWriter send, BufferedReader receive) {
                        try {
                            // save bufferedReader and bufferedWriter 
                            saveStream(receive, send);
                            sendMessage(send, new Gson().toJson(requestLogin()));
                            receiveMessage(receive);
                        } catch (IOException e) {
                            JOptionPane.showMessageDialog(null, "Error when connect to server: " + e.getMessage());
                        } catch (NullPointerException ex) {
                            // do nothing with error
                        }
                        lblImageLoading.setVisible(false);
                        btnLogin.setEnabled(true);
                    }
                });
            }).start();
        }
    }//GEN-LAST:event_btnLoginActionPerformed

    private void txtUsernameFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtUsernameFocusGained
        if (txtUsername.getText().equals("Nhập tên email")) {
            txtUsername.setText("");
            txtUsername.setForeground(Color.BLACK);
        }
    }//GEN-LAST:event_txtUsernameFocusGained

    private void txtUsernameFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtUsernameFocusLost
        if (txtUsername.getText().isEmpty()) {
            txtUsername.setForeground(new Color(204, 204, 204));
            txtUsername.setText("Nhập tên email");
        }
    }//GEN-LAST:event_txtUsernameFocusLost

    private void txtPasswordFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPasswordFocusGained
        if (txtPassword.getText().equals("Nhập mật khẩu")) {
            txtPassword.setText("");
            txtPassword.setEchoChar(passwordChar);
            txtPassword.setForeground(Color.BLACK);
        }
    }//GEN-LAST:event_txtPasswordFocusGained

    private void txtPasswordFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtPasswordFocusLost
        if (txtPassword.getPassword().length == 0) {
            txtPassword.setForeground(new Color(204, 204, 204));
            txtPassword.setEchoChar((char) 0);
            txtPassword.setText("Nhập mật khẩu");
        }
    }//GEN-LAST:event_txtPasswordFocusLost

    private void txtPasswordKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPasswordKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            btnLogin.doClick();
        }
    }//GEN-LAST:event_txtPasswordKeyPressed

    private void lblCreateAccountMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lblCreateAccountMouseClicked
        new RegisterForm().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_lblCreateAccountMouseClicked

    private void topPanelMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_topPanelMouseDragged
        this.setLocation(this.getX() + evt.getX() - mouseX, this.getY() + evt.getY() - mouseY);
    }//GEN-LAST:event_topPanelMouseDragged

    private void topPanelMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_topPanelMousePressed
        mouseX = evt.getX();
        mouseY = evt.getY();
    }//GEN-LAST:event_topPanelMousePressed

    private void btnSettingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnSettingMouseClicked
        //open setting form 
        new SettingForm().setVisible(true);
    }//GEN-LAST:event_btnSettingMouseClicked

    // validate all inputs
    private boolean validateInput() {

        if (txtUsername.getText().equals("Nhập email") || txtPassword.getText().equals("Nhập mật khẩu")) {
            JOptionPane.showMessageDialog(null, "Bạn chưa nhập đầy đủ thông tin.");
            txtUsername.requestFocus();
            lblImageLoading.setVisible(false);
            btnLogin.setEnabled(true);
            return false;
        } else if (!InputChecker.isEmail(username)) {
            JOptionPane.showMessageDialog(null, "Email không hợp lệ.");
            txtUsername.requestFocus();
            lblImageLoading.setVisible(false);
            btnLogin.setEnabled(true);
            return false;
        } else if (password.length() < 6) {
            JOptionPane.showMessageDialog(null, "Độ dài mật khẩu phải từ 6 ký tự.");
            txtPassword.requestFocus();
            lblImageLoading.setVisible(false);
            btnLogin.setEnabled(true);
            return false;
        } else {
            return true;
        }

    }

    // handle logic 
    private RequestModel requestLogin() {
        // using md5 library to hash password
        Gson gson = new Gson();
        Account account = new Account(txtUsername.getText().trim(), new MD5().getHashString(password.trim()));
        System.out.println(account.toString());
        return new RequestModel(Request.LOGIN, gson.toJson(account));
    }

    private void exitForm() {
        this.dispose();
    }

    private void saveStream(BufferedReader receive, BufferedWriter send) {
        this.receive = receive;
        this.send = send;
    }

    public BufferedReader getReceive() {
        return receive;
    }

    public BufferedWriter getSend() {
        return send;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new LoginForm().setVisible(true);
            }
        });
    }

    // custom variables
    private char passwordChar;
    private int mouseX;
    private int mouseY;
    private String username;
    private String password;
    // end of custom variables

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClose;
    private javax.swing.JButton btnLogin;
    private javax.swing.JButton btnMinimize;
    private javax.swing.JLabel btnSetting;
    private javax.swing.JLabel lblCreateAccount;
    private javax.swing.JLabel lblForm;
    private javax.swing.JLabel lblImageLoading;
    private javax.swing.JLabel lblPassword;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JLabel lblUsername;
    private javax.swing.JPanel mainPanel;
    private javax.swing.JPanel topPanel;
    private javax.swing.JPasswordField txtPassword;
    private javax.swing.JTextField txtUsername;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.GUI.ranklookup;

import client.GUI.waitingroom.ParingForm;
import client.GUI.waitingroom.WaitingRoomForm;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.net.ConnectException;
import java.net.UnknownHostException;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;
import utils.DTO.MatchResultDTO;
import utils.Encrypt.TripleDES;
import utils.Interfaces.Request;
import utils.Models.MatchResultModel;
import utils.Models.RequestModel;
import utils.Models.ResponseModel;
import utils.Models.UserDetailModel;

/**
 *
 * @author mvhix
 */
public class UserDetailForm extends JDialog{

    /**
     * Creates new form UserDetailForm
     */
    public UserDetailForm(String userId) {
        initComponents();
        customInitComponents();
        this.userId = userId;
    }

    //test
    public UserDetailForm() {
        initComponents();
        customInitComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblTitle = new javax.swing.JLabel();
        userDetailPanel = new javax.swing.JPanel();
        lblUserFullName = new javax.swing.JLabel();
        txtUserFullName = new javax.swing.JLabel();
        lblTotalMatches = new javax.swing.JLabel();
        txtTotalMatches = new javax.swing.JLabel();
        lblAccumulatePoint = new javax.swing.JLabel();
        txtAccumulatePoint = new javax.swing.JLabel();
        lblWinRate = new javax.swing.JLabel();
        txtWinRate = new javax.swing.JLabel();
        lblLoseRate = new javax.swing.JLabel();
        txtLoseRate = new javax.swing.JLabel();
        lblUserIQPoint = new javax.swing.JLabel();
        txtUserIQPoint = new javax.swing.JLabel();
        lblMaxChainOfWin = new javax.swing.JLabel();
        lblMaxChainOfLoss = new javax.swing.JLabel();
        txtMaxChainOfWin = new javax.swing.JLabel();
        txtMaxChainOfLoss = new javax.swing.JLabel();
        matchResultPanel = new javax.swing.JPanel();
        scrollPane = new javax.swing.JScrollPane();
        tblMatchResult = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Thông tin chi tiết");
        setAutoRequestFocus(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        lblTitle.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        lblTitle.setText("THÔNG TIN CỦA NGƯỜI CHƠI");

        userDetailPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Thông tin chi tiết", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 14))); // NOI18N

        lblUserFullName.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        lblUserFullName.setText("Họ và tên:");

        txtUserFullName.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        txtUserFullName.setText("Unknown uswer");

        lblTotalMatches.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        lblTotalMatches.setText("Tổng số trận đấu đã tham gia:");

        txtTotalMatches.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        txtTotalMatches.setText("0");

        lblAccumulatePoint.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        lblAccumulatePoint.setText("Điểm tích lũy:");

        txtAccumulatePoint.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        txtAccumulatePoint.setText("0");

        lblWinRate.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        lblWinRate.setText("Tỉ lệ thắng:");

        txtWinRate.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        txtWinRate.setText("50%");

        lblLoseRate.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        lblLoseRate.setText("Tỉ lệ thua:");

        txtLoseRate.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        txtLoseRate.setText("50%");

        lblUserIQPoint.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        lblUserIQPoint.setText("Chỉ số IQ:");

        txtUserIQPoint.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        txtUserIQPoint.setText("0");

        lblMaxChainOfWin.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        lblMaxChainOfWin.setText("Chuỗi thắng dài nhất: ");

        lblMaxChainOfLoss.setFont(new java.awt.Font("Segoe UI", 0, 16)); // NOI18N
        lblMaxChainOfLoss.setText("Chuỗi thua dài nhất: ");

        txtMaxChainOfWin.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        txtMaxChainOfWin.setText("50%");

        txtMaxChainOfLoss.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        txtMaxChainOfLoss.setText("50%");

        javax.swing.GroupLayout userDetailPanelLayout = new javax.swing.GroupLayout(userDetailPanel);
        userDetailPanel.setLayout(userDetailPanelLayout);
        userDetailPanelLayout.setHorizontalGroup(
            userDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(userDetailPanelLayout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(userDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(userDetailPanelLayout.createSequentialGroup()
                        .addComponent(lblAccumulatePoint)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtAccumulatePoint))
                    .addGroup(userDetailPanelLayout.createSequentialGroup()
                        .addComponent(lblTotalMatches)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotalMatches))
                    .addGroup(userDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, userDetailPanelLayout.createSequentialGroup()
                            .addComponent(lblUserIQPoint)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtUserIQPoint))
                        .addGroup(userDetailPanelLayout.createSequentialGroup()
                            .addComponent(lblUserFullName)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtUserFullName)))
                    .addGroup(userDetailPanelLayout.createSequentialGroup()
                        .addGroup(userDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(userDetailPanelLayout.createSequentialGroup()
                                .addComponent(lblWinRate)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtWinRate))
                            .addGroup(userDetailPanelLayout.createSequentialGroup()
                                .addComponent(lblLoseRate)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtLoseRate)))
                        .addGap(40, 40, 40)
                        .addGroup(userDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(userDetailPanelLayout.createSequentialGroup()
                                .addComponent(lblMaxChainOfLoss)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtMaxChainOfLoss))
                            .addGroup(userDetailPanelLayout.createSequentialGroup()
                                .addComponent(lblMaxChainOfWin)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtMaxChainOfWin)))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        userDetailPanelLayout.setVerticalGroup(
            userDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(userDetailPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(userDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUserFullName)
                    .addComponent(txtUserFullName))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(userDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblUserIQPoint)
                    .addComponent(txtUserIQPoint))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(userDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblAccumulatePoint)
                    .addComponent(txtAccumulatePoint))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(userDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblTotalMatches)
                    .addComponent(txtTotalMatches))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(userDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblWinRate)
                    .addComponent(txtWinRate)
                    .addComponent(lblMaxChainOfWin)
                    .addComponent(txtMaxChainOfWin))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(userDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtMaxChainOfLoss)
                    .addGroup(userDetailPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblLoseRate)
                        .addComponent(txtLoseRate)
                        .addComponent(lblMaxChainOfLoss)))
                .addContainerGap())
        );

        matchResultPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Kết quả các trận đã tham gia", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Segoe UI", 1, 14))); // NOI18N

        tblMatchResult.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        scrollPane.setViewportView(tblMatchResult);

        javax.swing.GroupLayout matchResultPanelLayout = new javax.swing.GroupLayout(matchResultPanel);
        matchResultPanel.setLayout(matchResultPanelLayout);
        matchResultPanelLayout.setHorizontalGroup(
            matchResultPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, matchResultPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(scrollPane, javax.swing.GroupLayout.PREFERRED_SIZE, 447, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        matchResultPanelLayout.setVerticalGroup(
            matchResultPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, matchResultPanelLayout.createSequentialGroup()
                .addComponent(scrollPane, javax.swing.GroupLayout.DEFAULT_SIZE, 193, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lblTitle, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(matchResultPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(userDetailPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(lblTitle)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(userDetailPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(matchResultPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        requestLoadUserDetail();
    }//GEN-LAST:event_formWindowOpened

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
    }//GEN-LAST:event_formWindowClosed

    private void customInitComponents() {
        this.setLocationRelativeTo(null);

        setModal(true);
        setModalityType(ModalityType.APPLICATION_MODAL);
        
        lblTitle.setHorizontalAlignment(SwingConstants.CENTER);

        // table's configurations
        tblModel = new DefaultTableModel(new String[]{"Số thứ tự", "Số điểm",
            "Kết quả"}, 0) {
            @Override
            public boolean isCellEditable(int row, int column) {
                // all cells false
                return false;
            }
        };

        tblMatchResult.setModel(tblModel);
        tblMatchResult.setFillsViewportHeight(true);
        tblMatchResult.setRowHeight(30);

        // center cells
        DefaultTableCellRenderer cellRenderer = new DefaultTableCellRenderer();
        cellRenderer.setHorizontalAlignment(SwingConstants.CENTER);

        int columns = tblModel.getColumnCount();

        for (int columnIndex = 0; columnIndex < columns; columnIndex++) {
            tblMatchResult.getColumnModel().getColumn(columnIndex).setCellRenderer(cellRenderer);
        }
    }

    private void sendMessage(BufferedWriter send, String message) throws IOException {
        String encryptData = new TripleDES().encrypt(message);
        send.write(encryptData);
        send.newLine();
        send.flush();
    }

    private void receiveMessage(BufferedReader receive) throws IOException {
        String response = new TripleDES().decrypt(receive.readLine());
        ResponseModel responseModel = new Gson().fromJson(response, ResponseModel.class);
        if (responseModel.getType().equals(Request.GET_USER_DETAIL)) {
            if (responseModel.isStatus()) {
                loadData(responseModel.getData());
            } else {
                JOptionPane.showMessageDialog(null, responseModel.getData());
            }
        }
    }

    private void requestLoadUserDetail() {
        try {
            sendMessage(WaitingRoomForm.send, new Gson().toJson(new RequestModel(Request.GET_USER_DETAIL, userId)));
            receiveMessage(WaitingRoomForm.receive);
        } catch (UnknownHostException e) {
            System.out.println("Host không hợp lệ");
            JOptionPane.showMessageDialog(null, "Không tồn tại địa chỉ host để kết nối đến server");
        } catch (ConnectException e) {
            System.out.println("Server đang offline vui lòng truy cập lại sau: " + e.getMessage());
            JOptionPane.showMessageDialog(null, "Vui lòng kiểm tra lại kết nối internet");
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Không có kết nối internet");
            this.dispose();
        } catch (NullPointerException e) {
            // do nothing 
        }
    }

    private void loadData(String data) { 

        Gson gson = new Gson();
        UserDetailModel model = gson.fromJson(data, UserDetailModel.class);

        //set value
        txtUserFullName.setText(model.getMember().getFullName());
        txtUserIQPoint.setText(String.valueOf(model.getMember().getIQPoint()));
        txtAccumulatePoint.setText(String.valueOf(model.getMember().getAccumulatePoint()));
        txtTotalMatches.setText(String.valueOf(model.getTotalMatch()));
        txtWinRate.setText(model.getWinRate());
        txtLoseRate.setText(model.getLossRate());
        txtMaxChainOfWin.setText(model.getMaxChainOfWin());
        txtMaxChainOfLoss.setText(model.getMaxChainOfLoss());

        List<MatchResultModel> results = model.getMatchResult();

        for (int i = 0; i < results.size(); i++) {
            // row
            Vector row = new Vector();

            row.add((i + 1));  // ordinal number
            row.add(results.get(i).getTotal_point());
            row.add(results.get(i).isResult() ? "Thắng" : "Thua");  // match result
            tblModel.addRow(row);
        }

        tblMatchResult.setModel(tblModel);

        // set row sorter
        rowSorter = new TableRowSorter<TableModel>(tblMatchResult.getModel());
        rowSorter.setComparator(0, Comparator.naturalOrder());
        rowSorter.setComparator(1, Comparator.naturalOrder());
        tblMatchResult.setRowSorter(rowSorter);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new UserDetailForm().setVisible(true);
            }
        });
    }

    // custom variables
    private String userId;
    private DefaultTableModel tblModel;
    private TableRowSorter<TableModel> rowSorter;
    private volatile boolean haveInternet = false;
    private volatile ExecutorService service = Executors.newCachedThreadPool();
    ;
    
    // end of custom variables

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel lblAccumulatePoint;
    private javax.swing.JLabel lblLoseRate;
    private javax.swing.JLabel lblMaxChainOfLoss;
    private javax.swing.JLabel lblMaxChainOfWin;
    private javax.swing.JLabel lblTitle;
    private javax.swing.JLabel lblTotalMatches;
    private javax.swing.JLabel lblUserFullName;
    private javax.swing.JLabel lblUserIQPoint;
    private javax.swing.JLabel lblWinRate;
    private javax.swing.JPanel matchResultPanel;
    private javax.swing.JScrollPane scrollPane;
    private javax.swing.JTable tblMatchResult;
    private javax.swing.JLabel txtAccumulatePoint;
    private javax.swing.JLabel txtLoseRate;
    private javax.swing.JLabel txtMaxChainOfLoss;
    private javax.swing.JLabel txtMaxChainOfWin;
    private javax.swing.JLabel txtTotalMatches;
    private javax.swing.JLabel txtUserFullName;
    private javax.swing.JLabel txtUserIQPoint;
    private javax.swing.JLabel txtWinRate;
    private javax.swing.JPanel userDetailPanel;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package client.Socket;

import utils.Interfaces.Action;
import utils.Env.Const;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ConnectException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;
import java.net.UnknownHostException;
import javax.swing.JOptionPane;
import utils.Common.SocketExtension;

/**
 *
 * @author nguyen huu duc
 */
public class ClientManager {

    private Socket socket;
    private BufferedReader receive;
    private BufferedWriter send;

    private static volatile ClientManager INSTANCE = null;

    public static ClientManager getInstance() {
        if (INSTANCE == null) {
            synchronized (ClientManager.class) {
                if (INSTANCE == null) {
                    INSTANCE = new ClientManager();

                }
            }
        }
        return INSTANCE;
    }

    private ClientManager() {
    }

    public void runClient(Action action) {
        System.out.println("==========Client side=========");
        openConnection();
        action.executeAction(send, receive);
        //closeConnection();
    }

    public void openConnection() {
        try {
            socket = new Socket(Const.HOSTNAME, Const.PORT);
            socket.setTcpNoDelay(true);
            socket.setOOBInline(true);

            receive = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            send = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            System.out.println("Client is listenning on port: " + socket.getLocalPort());
            System.out.println("Connect to server: " + socket.getRemoteSocketAddress());
        } catch (UnknownHostException e) {
            System.out.println("Host không hợp lệ");
            JOptionPane.showMessageDialog(null, "Không tồn tại địa chỉ host để kết nối đến server");
        } catch (ConnectException e) {
            System.out.println("Server đang offline vui lòng truy cập lại sau: " + e.getMessage());
            JOptionPane.showMessageDialog(null, "Không kết nối được với server vui lòng kiểm tra lại kết nối internet, địa chỉ host và port để kết nối đến server");
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (NullPointerException e) {
            // do nothing 
        } catch(Exception e) {
            JOptionPane.showMessageDialog(null, "Không có kết nối internet vui lòng kiểm tra lại đường truyền");
        }
    }

    public void closeConnection() {
        try {
            if (receive != null) {
                receive.close();
            }
            if (send != null) {
                send.close();
            }
            if (socket.isConnected() && socket != null) {
                socket.close();
            }
            System.out.println("Client quit!");
        } catch (IOException ex) {
            System.out.println("Error closing: " + ex.getMessage());
        } catch (NullPointerException e) {
            System.out.println("Socket not open to close");
        }
    }

    public Socket getSocket() {
        return this.socket;
    }

}

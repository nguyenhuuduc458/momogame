-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 22, 2020 lúc 11:35 AM
-- Phiên bản máy phục vụ: 10.4.14-MariaDB
-- Phiên bản PHP: 7.3.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `momogame`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tblanswer`
--

CREATE TABLE `tblanswer` (
  `id` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL,
  `is_true` tinyint(1) NOT NULL DEFAULT 0,
  `question_id` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tblanswer`
--

INSERT INTO `tblanswer` (`id`, `content`, `is_true`, `question_id`) VALUES
('0030e40c-878f-4923-b7ab-7ad72b96d69c', 'Coimbra', 0, '5536dbc7-25df-40c3-a79d-11d5832fe7ba'),
('019712f9-1bca-43c3-b941-7a50cf655ef4', 'V', 0, 'a9c071e8-dabc-4ac6-ac3a-ef6358b026e0'),
('01dd2d14-3f7a-467b-a970-42ea0cf9feb7', 'Oliver', 0, '1786c686-c8a3-4d90-a273-e194f24cc6ac'),
('04eeda9f-d0b6-47eb-868e-9c8756ddb086', 'Ngôi sao', 0, '1cf06f4a-caef-4af0-a660-8413b10aa02a'),
('053592a0-d3ee-4e64-a4a2-ae978a575b5b', 'Kimber', 0, '1786c686-c8a3-4d90-a273-e194f24cc6ac'),
('06bc2bf3-2388-449c-9c25-286c39edf879', 'Bóng chày', 0, '8132654a-14b6-42fc-a985-92500c3c4615'),
('0ce224a4-afea-42cc-97b3-840dbcfabf41', 'Wave', 0, '1786c686-c8a3-4d90-a273-e194f24cc6ac'),
('111a8859-84f4-40b1-bff1-273834df534d', 'Emma Stone', 1, '45ac6311-5b3b-4845-a27f-fafae44d01d1'),
('13f49659-c8de-4c8e-9a00-f732dda83b98', 'SK Group', 0, '10896e76-a97b-404c-b1ec-e6b40b1a107b'),
('163f2518-f78b-4f7b-88b4-9ee18f4247ee', 'Emma Roberts', 0, '45ac6311-5b3b-4845-a27f-fafae44d01d1'),
('18c2c6ad-b344-4d41-8196-71f42a1e5712', 'Charlie Puth', 0, 'b41ce9d2-7459-4078-82e3-f5403c12cd1d'),
('1cd192d3-ce5b-4f28-8b7f-4236c76a5bcd', 'Tomorrowland', 0, '368aeee0-583a-4e93-bfc7-dad3af09ddd6'),
('2028aaa9-4022-4c51-be5e-6e5bbc796218', '58', 0, 'cf559318-79a8-4c06-86dd-d5820ead0ec7'),
('20b8c12d-7c80-43b4-85f5-832f7114d0f5', 'Nhiễm sắc thể số 1', 1, 'fc2a2632-8913-4e2d-9980-5105b52aa1a1'),
('23108747-0c36-4287-bc3a-29e13819ce00', '60', 0, 'cf559318-79a8-4c06-86dd-d5820ead0ec7'),
('239c3c09-572a-49d6-9492-752d0ba3d253', 'Barbara', 0, '2c53e607-172e-4194-ba03-dd4476dcbd40'),
('24ecd18b-aa44-4374-a02f-cfcea849e5c6', 'Luis Figo', 0, 'd79079b1-bf3e-47b8-ba30-4aacbb6b0737'),
('24fbce2c-db95-42a3-88e4-65bc3ef6fc1c', 'Hải Phòng', 0, '1ba38221-b644-4b77-b7d7-323024e39b3b'),
('2bff007c-eb60-489c-ac71-c178a77a74bd', 'Nhiễm sắc thể số 20', 0, 'fc2a2632-8913-4e2d-9980-5105b52aa1a1'),
('2ffbe95d-3f4c-4025-9dac-0d9c91e30b27', 'Westlife', 1, '4241c073-ef1d-4484-8ed3-369a4a4f7bd4'),
('301c3b06-520f-4daa-b0d0-85f85a60136d', 'Cú đêm', 0, '59726c8d-84e4-43ae-8436-b4b4e5e9df2c'),
('33ff603d-614d-4514-923d-af0cd565a1ff', 'Ngày quốc khánh', 0, 'a31e6a87-4c92-4e0b-8e3b-7f5a57671e65'),
('349383c4-e929-44e7-90ec-72c597437ea1', 'Hồ Chí Minh', 0, '1ba38221-b644-4b77-b7d7-323024e39b3b'),
('3912b047-3163-4ab2-94e5-18f4216b7a85', 'Vàng', 0, 'fbda1031-32a5-42f8-a0a1-1eb4da82018e'),
('39775c13-0cb4-4f61-9a27-4b11eeebba68', '1913', 0, '6d51deb4-a9ff-4a65-a8e0-e9887e1a70c2'),
('3cadc97d-caaa-4256-9a56-74bdd02de983', '1', 0, '580ef4f3-dbc8-4065-87c3-ece219f88e9c'),
('3cc9194a-eaba-4588-97db-ff2c0c459355', 'Lewis', 0, '2c53e607-172e-4194-ba03-dd4476dcbd40'),
('3f1165ed-e495-4de8-85ac-404729a519e4', 'LG', 0, '10896e76-a97b-404c-b1ec-e6b40b1a107b'),
('41b69843-f986-4601-aefb-f9ce164e5d81', 'Lisbon', 1, '5536dbc7-25df-40c3-a79d-11d5832fe7ba'),
('437aa017-f59f-459d-aba3-611ebaf2c358', '1914', 0, '6d51deb4-a9ff-4a65-a8e0-e9887e1a70c2'),
('4577f4f5-b3e6-4c03-9ba6-e41eab1ac9a3', '26 giờ', 0, '092e6e51-62a0-463e-855e-011e5d768628'),
('46d7bfdd-0ae7-4f7d-84d4-a88a5efba128', '40,000 mỗi ngày', 0, '9e42bf07-a4f9-4c38-a41d-82c0989e1eda'),
('54604159-720e-40bf-8a4e-133057f6a773', 'Bóng bầu dục', 0, '8132654a-14b6-42fc-a985-92500c3c4615'),
('5db5741d-bf1f-4106-8708-4bdbb3e1d939', '1973', 0, 'c25478bd-edea-4a19-ae03-009a47b7dc54'),
('5ecbb234-7f33-436a-98ef-23d35880f77b', 'Lào', 0, 'f7791d45-ad4c-4660-8060-f04c72ac176a'),
('63c44dfd-9741-4960-996a-4cd6ca9c2611', 'Bước sóng 510nm', 0, '1cf06f4a-caef-4af0-a660-8413b10aa02a'),
('6afd164f-486c-42ad-9dc2-0c8d1d6fa612', 'Con vịt', 0, '102f8398-eee6-4be7-921b-ac6f9ff25db0'),
('6d4e8ef0-6c91-40eb-bf5d-59ad34d9e48d', 'Thái Lan', 1, 'f7791d45-ad4c-4660-8060-f04c72ac176a'),
('6eb15b12-fad9-4c1e-b525-8c3e865e3045', 'Đồng', 0, 'fbda1031-32a5-42f8-a0a1-1eb4da82018e'),
('731af354-46af-4a74-8a0a-0034689eef37', '1970', 0, 'c25478bd-edea-4a19-ae03-009a47b7dc54'),
('73c35940-a1c7-4fda-bcf4-73409393bdfb', 'Câu trả lời 1 cho câu hỏi 1\'s', 1, '580ef4f3-dbc8-4065-87c3-ece219f88e9c'),
('760d5f1c-fa97-4eae-b4e2-45f4293f1ddf', 'NSYNC', 0, '4241c073-ef1d-4484-8ed3-369a4a4f7bd4'),
('76a8325d-5d0a-4699-918a-ae2a33bb6818', 'Ngày lễ hòa bình', 0, 'a31e6a87-4c92-4e0b-8e3b-7f5a57671e65'),
('792ef8bb-e4c2-4db6-9a96-af8dcab5dfa3', 'Cua hoàng đế', 1, '59726c8d-84e4-43ae-8436-b4b4e5e9df2c'),
('7a5ea8a1-e3e5-41c6-b2e0-de0beee5ae2e', 'Ngày độc lập ', 0, 'a31e6a87-4c92-4e0b-8e3b-7f5a57671e65'),
('7ceb7691-b37b-4bd9-a648-f6fdaf5310f3', ' Shawy', 1, '2c53e607-172e-4194-ba03-dd4476dcbd40'),
('7f43b710-ab8e-4dbd-9e6f-0293a98811ac', '25 giờ', 0, '092e6e51-62a0-463e-855e-011e5d768628'),
('812297ed-d43c-4c43-9058-f51f760ee8a5', '1915', 0, '6d51deb4-a9ff-4a65-a8e0-e9887e1a70c2'),
('81474e7d-cd69-48a2-8cba-4b2bc4208746', 'Óbidos', 0, '5536dbc7-25df-40c3-a79d-11d5832fe7ba'),
('84e80a59-c14d-42ef-a059-b7bc7489500c', '1972', 1, 'c25478bd-edea-4a19-ae03-009a47b7dc54'),
('862997bd-414d-4063-810a-7f12e4cbadf8', 'S', 1, 'a9c071e8-dabc-4ac6-ac3a-ef6358b026e0'),
('872e9c18-fbe6-4d09-a698-cfa63064e37e', 'Bóng đá', 0, '8132654a-14b6-42fc-a985-92500c3c4615'),
('8cce60b0-21dd-48ac-ba87-1fc921f0e6fb', 'Ngày môi trường thế giới ', 1, 'a31e6a87-4c92-4e0b-8e3b-7f5a57671e65'),
('8f54ed04-e3d0-479f-8e5d-63f51e34402d', 'Nhiễm sắc thể số 21', 0, 'fc2a2632-8913-4e2d-9980-5105b52aa1a1'),
('90f76e5c-8030-40b7-bf52-065cf9a67bae', 'Mang theo Sergeant', 1, '368aeee0-583a-4e93-bfc7-dad3af09ddd6'),
('9316e6c9-0676-4253-b7c2-f327b0d2f009', 'Con chó', 1, '102f8398-eee6-4be7-921b-ac6f9ff25db0'),
('9433023a-973e-4861-9992-0a0d4398a6c5', 'Ag', 1, 'b7ae6dff-3181-4315-a196-5ccfecfc8fb3'),
('95f471ce-3ada-4290-bea1-506a85fb56fd', '54', 1, 'cf559318-79a8-4c06-86dd-d5820ead0ec7'),
('98243c33-942e-4668-837f-25c8dbd74eae', 'Bóng rổ', 1, '8132654a-14b6-42fc-a985-92500c3c4615'),
('9a5305d2-75ad-4bd6-940c-912f1e0bb7a2', '2', 0, '580ef4f3-dbc8-4065-87c3-ece219f88e9c'),
('9c5f3dc3-2da5-4c03-bac7-0590c4d77c18', 'Charli XCX', 0, 'b41ce9d2-7459-4078-82e3-f5403c12cd1d'),
('9e72e141-930b-4b6f-86c5-c9cb7e764d8f', 'Con gà', 0, '102f8398-eee6-4be7-921b-ac6f9ff25db0'),
('a4fbb357-ad1f-4ee7-8d29-339061ad02d4', 'Justin Bieber', 1, 'b41ce9d2-7459-4078-82e3-f5403c12cd1d'),
('a939ad0d-87e6-4acb-b675-0ed0f8cd1d02', 'Hyundai Heavy Industries', 0, '10896e76-a97b-404c-b1ec-e6b40b1a107b'),
('ac8ba787-be44-47d0-a50a-e51c5f6c9c8d', '2 năm', 0, '092e6e51-62a0-463e-855e-011e5d768628'),
('adff39bc-1a4c-42d2-94c7-66aa5eeaaf9b', 'Al', 0, 'b7ae6dff-3181-4315-a196-5ccfecfc8fb3'),
('ae5c8b5d-3a7b-46de-9fe2-7892ad7db402', 'Nhôm', 1, 'fbda1031-32a5-42f8-a0a1-1eb4da82018e'),
('b13d46d1-0b40-4844-964c-5daeb302cdd5', 'Roberto Carlos', 0, 'd79079b1-bf3e-47b8-ba30-4aacbb6b0737'),
('b3ce5cdf-7e4e-404a-8deb-9ff3049e8a3e', '2,6 triệu năm ánh sáng', 1, '1cf06f4a-caef-4af0-a660-8413b10aa02a'),
('b543c931-ae16-48de-8abc-d119248a4235', 'Robert Peel', 0, '2c53e607-172e-4194-ba03-dd4476dcbd40'),
('b7e1dc63-a9cb-4709-9b11-c1157257982f', 'Campuchia', 0, 'f7791d45-ad4c-4660-8060-f04c72ac176a'),
('b9823951-abec-4b4d-a2c1-5ecbcc48068e', 'Justin Timberlake', 0, 'b41ce9d2-7459-4078-82e3-f5403c12cd1d'),
('bb95b123-8e19-4509-915b-e4fd12651ddd', 'Hg', 0, 'b7ae6dff-3181-4315-a196-5ccfecfc8fb3'),
('bcf99ea5-b1fc-492c-9141-60d04aed810d', 'Samsung', 1, '10896e76-a97b-404c-b1ec-e6b40b1a107b'),
('beb460c2-5c3e-4f8b-8578-330adeac514e', '50,000 mỗi ngày', 0, '9e42bf07-a4f9-4c38-a41d-82c0989e1eda'),
('c0596215-f59e-4417-be33-000a68c3c45d', 'Huế', 0, '1ba38221-b644-4b77-b7d7-323024e39b3b'),
('c340bd3b-9e63-428a-be39-6cc9c0ef9e20', 'Zinedine Zidane', 0, 'd79079b1-bf3e-47b8-ba30-4aacbb6b0737'),
('c8e3484d-ba17-4d63-bc49-70ec8975953e', 'Backstreet Boys', 0, '4241c073-ef1d-4484-8ed3-369a4a4f7bd4'),
('ca8e7723-96a1-42ab-b8f6-1fd57afc6aff', 'Đại bàng', 0, '59726c8d-84e4-43ae-8436-b4b4e5e9df2c'),
('caf30cc1-f715-4cc5-94b5-48e98bd6bc62', '3', 0, '580ef4f3-dbc8-4065-87c3-ece219f88e9c'),
('cc42aaa5-3689-4fe1-a78f-269bbd8b89a5', '30,000 mỗi ngày', 0, '9e42bf07-a4f9-4c38-a41d-82c0989e1eda'),
('ccaaa81e-d504-415d-8226-960d1ee0df37', 'Peter Durand', 1, '1786c686-c8a3-4d90-a273-e194f24cc6ac'),
('cd4a3dd0-f5ee-4e86-b11a-db206b584dc7', 'Emma Waston', 0, '45ac6311-5b3b-4845-a27f-fafae44d01d1'),
('d05d38ea-98f1-405e-a1b5-a642eb4c44ee', 'Việt Nam', 0, 'f7791d45-ad4c-4660-8060-f04c72ac176a'),
('d5c163a9-73c0-4d4f-ae2a-c42cc952eadd', '1912', 1, '6d51deb4-a9ff-4a65-a8e0-e9887e1a70c2'),
('d97f18ae-3cbd-438b-94a2-3cca156280c4', 'Đường chân trời', 0, '1cf06f4a-caef-4af0-a660-8413b10aa02a'),
('db017751-26ee-402f-aa49-5dfeb5bc87ab', 'Porto', 0, '5536dbc7-25df-40c3-a79d-11d5832fe7ba'),
('dd1e5b2a-240f-4aec-8da0-6f9702b6c1bc', 'K', 0, 'a9c071e8-dabc-4ac6-ac3a-ef6358b026e0'),
('de70f628-8aa8-43e6-aa49-65de78b3bfb3', 'David Beckham', 1, 'd79079b1-bf3e-47b8-ba30-4aacbb6b0737'),
('e0f44168-bcf1-4758-8939-06a0c7b4e596', '1969', 0, 'c25478bd-edea-4a19-ae03-009a47b7dc54'),
('e18b1c99-78db-4c49-aec0-e6be4745b250', 'Cá mú', 0, '59726c8d-84e4-43ae-8436-b4b4e5e9df2c'),
('e44aed7c-c8a1-46af-964b-78c64019ce3c', '20,000 mỗi ngày', 1, '9e42bf07-a4f9-4c38-a41d-82c0989e1eda'),
('e8e8231d-344b-483a-a1fb-90561e3a0661', 'Kẻ hủy diệt', 0, '368aeee0-583a-4e93-bfc7-dad3af09ddd6'),
('ea34ade9-aa1e-42bf-a055-349d696646be', 'Au', 0, 'b7ae6dff-3181-4315-a196-5ccfecfc8fb3'),
('eb813c8c-c935-427c-8ad1-6f67c48e0f73', 'Chì', 0, 'fbda1031-32a5-42f8-a0a1-1eb4da82018e'),
('ed261426-5dd4-4709-b97f-b034fdb8137d', '24 giờ', 1, '092e6e51-62a0-463e-855e-011e5d768628'),
('edc5b552-a264-44f7-957e-3ef328649838', 'Boyz II Men', 0, '4241c073-ef1d-4484-8ed3-369a4a4f7bd4'),
('f1bc23f6-138a-4db7-9ffb-3f8eaffac9a6', '64', 0, 'cf559318-79a8-4c06-86dd-d5820ead0ec7'),
('f312a190-614e-4490-b518-29eda8a644a1', 'Hà Nội', 1, '1ba38221-b644-4b77-b7d7-323024e39b3b'),
('f39cea1e-3851-45a3-b9c6-bb595ec23ad2', 'U', 0, 'a9c071e8-dabc-4ac6-ac3a-ef6358b026e0'),
('f708ffd1-83d9-4820-8c70-09e91520ce34', 'Còn mèo', 0, '102f8398-eee6-4be7-921b-ac6f9ff25db0'),
('f726f198-d5cd-4ea8-9ae1-d2ef79a2d4e6', 'Emma Thompson', 0, '45ac6311-5b3b-4845-a27f-fafae44d01d1'),
('fbfeab7b-53b7-4c30-afcb-b404f2f509b5', 'Deadpool', 0, '368aeee0-583a-4e93-bfc7-dad3af09ddd6'),
('fcc4afed-739d-4e98-9c31-12ad4d3a4a8c', 'Nhiêm sắc số 9', 0, 'fc2a2632-8913-4e2d-9980-5105b52aa1a1');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tblmatch`
--

CREATE TABLE `tblmatch` (
  `id` varchar(255) NOT NULL,
  `match_name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tblmatchresult`
--

CREATE TABLE `tblmatchresult` (
  `id` varchar(255) NOT NULL,
  `member_id` varchar(255) NOT NULL,
  `match_id` varchar(255) NOT NULL,
  `date` varchar(255) NOT NULL,
  `total_point` int(255) UNSIGNED NOT NULL,
  `result` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tblmember`
--

CREATE TABLE `tblmember` (
  `id` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `sex` varchar(10) NOT NULL,
  `birthday` varchar(255) NOT NULL,
  `iq_point` smallint(5) UNSIGNED NOT NULL DEFAULT 0,
  `accumulate_point` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `is_active` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tblmember`
--

INSERT INTO `tblmember` (`id`, `username`, `password`, `firstname`, `lastname`, `sex`, `birthday`, `iq_point`, `accumulate_point`, `is_active`) VALUES
('0a6a4ba9-7094-44ba-8e45-e3f57cd2044b', 'nguyenhuuduc225@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Phạm Tuyết', 'Quỳnh Như', 'Nữ', '19/11/2020', 0, 0, 1),
('f35734cf-0b30-4208-8a4a-dc3490a51d33', 'pmhien2703@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Phạm', 'Minh Hiển', 'Nam', '23/10/2020', 0, 0, 1),
('f906091f-41f4-4c9e-832e-fd68bed61253', 'trunghieu9946@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'Lê', 'Viết', 'Nam', '07/11/2020', 1, 0, 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tblquestion`
--

CREATE TABLE `tblquestion` (
  `id` varchar(255) NOT NULL,
  `content` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tblquestion`
--

INSERT INTO `tblquestion` (`id`, `content`) VALUES
('6d51deb4-a9ff-4a65-a8e0-e9887e1a70c2', ' Năm Titanic chìm ở Đại Tây Dương vào ngày 15 tháng XNUMX, trong chuyến đi đầu tiên từ Southampton ?'),
('c25478bd-edea-4a19-ae03-009a47b7dc54', '\"The Godfather\" được phát hành lần đầu tiên vào năm nào'),
('d79079b1-bf3e-47b8-ba30-4aacbb6b0737', 'Ai là \"Nhân vật thể thao của năm\" của BBC năm 2001'),
('1786c686-c8a3-4d90-a273-e194f24cc6ac', 'Ai đã phát minh ra hộp thiếc để bảo quản thực phẩm vào năm 1810?'),
('2c53e607-172e-4194-ba03-dd4476dcbd40', 'Ai đã phát minh ra Mắt mèo vào năm 1934 để cải thiện an toàn đường bộ?'),
('4241c073-ef1d-4484-8ed3-369a4a4f7bd4', 'Bài hát \"My Love\" là một trong những bài hát nổi tiếng nhất của nhóm nam nào'),
('1cf06f4a-caef-4af0-a660-8413b10aa02a', 'Cái gì xa nhất bạn có thể nhìn thấy bằng mắt thường'),
('580ef4f3-dbc8-4065-87c3-ece219f88e9c', 'Câu hỏi 1 \'s'),
('9e42bf07-a4f9-4c38-a41d-82c0989e1eda', 'Cơ thể con người mất bao nhiêu hơi thở hàng ngày ?'),
('59726c8d-84e4-43ae-8436-b4b4e5e9df2c', 'Con gì sinh ra đã là vua ?'),
('102f8398-eee6-4be7-921b-ac6f9ff25db0', 'Con vật đầu tiên bay vào vũ trụ là con gì ?'),
('8132654a-14b6-42fc-a985-92500c3c4615', 'James Naismith đã phát minh ra trò chơi thể thao nào vào năm 1891'),
('fbda1031-32a5-42f8-a0a1-1eb4da82018e', 'Kim loại nào được Hans Christian Oersted phát hiện vào năm 1825 ?'),
('b7ae6dff-3181-4315-a196-5ccfecfc8fb3', 'Ký hiệu hóa học cho bạc là gì'),
('f7791d45-ad4c-4660-8060-f04c72ac176a', 'Lễ hội té nước là lễ hội của nước nào?'),
('a31e6a87-4c92-4e0b-8e3b-7f5a57671e65', 'Ngày 05/06 là ngày lễ gì trên thế giới?'),
('b41ce9d2-7459-4078-82e3-f5403c12cd1d', 'Ngôi sao nhạc pop nào của Mỹ đã trở lại thành công trên bảng xếp hạng năm 2015 với đĩa đơn \"Sorry\" và \"Love Yourself\"'),
('45ac6311-5b3b-4845-a27f-fafae44d01d1', 'Nữ diễn viên chính đạt giải Oscar trong phim \"La La Land\" tên là gì'),
('fc2a2632-8913-4e2d-9980-5105b52aa1a1', 'Phân tử lớn nhất tạo thành một phần của cơ thể con người là gì'),
('10896e76-a97b-404c-b1ec-e6b40b1a107b', 'Tên của công ty công nghệ lớn nhất ở Hàn Quốc là gì'),
('5536dbc7-25df-40c3-a79d-11d5832fe7ba', 'Thủ đô của Bồ Đào Nha là gì?'),
('1ba38221-b644-4b77-b7d7-323024e39b3b', 'Thủ đô của Việt Nam là thành phố nào'),
('368aeee0-583a-4e93-bfc7-dad3af09ddd6', 'Tiêu đề của bộ phim Carry On đầu tiên được sản xuất và phát hành năm 1958 là gì ?'),
('092e6e51-62a0-463e-855e-011e5d768628', 'Tuổi thọ của chuồn chuồn là gì?'),
('cf559318-79a8-4c06-86dd-d5820ead0ec7', 'Việt Nam có bao nhiêu dân tộc'),
('a9c071e8-dabc-4ac6-ac3a-ef6358b026e0', 'Đất nước Việt Nam hình chữ gì ?');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tblrule`
--

CREATE TABLE `tblrule` (
  `id` varchar(255) NOT NULL,
  `content` varchar(50) NOT NULL,
  `value` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Đang đổ dữ liệu cho bảng `tblrule`
--

INSERT INTO `tblrule` (`id`, `content`, `value`) VALUES
('1d242de8-7177-4dd0-a349-db84ee158e21', 'QUESTION_NUMBER', 5),
('b08e02e7-e9d0-493e-93d1-2193031d0d1b', 'TIME_ANSWER', 10),
('c1dfbe52-ef49-4841-9e16-130567472be5', 'POINT_PLUS', 80);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tblanswer`
--
ALTER TABLE `tblanswer`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_answer_question` (`question_id`);

--
-- Chỉ mục cho bảng `tblmatch`
--
ALTER TABLE `tblmatch`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tblmatchresult`
--
ALTER TABLE `tblmatchresult`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_matchresult_member` (`member_id`),
  ADD KEY `fk_matchresult_match` (`match_id`);

--
-- Chỉ mục cho bảng `tblmember`
--
ALTER TABLE `tblmember`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Chỉ mục cho bảng `tblquestion`
--
ALTER TABLE `tblquestion`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `content` (`content`);

--
-- Chỉ mục cho bảng `tblrule`
--
ALTER TABLE `tblrule`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `content` (`content`);

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `tblanswer`
--
ALTER TABLE `tblanswer`
  ADD CONSTRAINT `fk_answer_question` FOREIGN KEY (`question_id`) REFERENCES `tblquestion` (`id`);

--
-- Các ràng buộc cho bảng `tblmatchresult`
--
ALTER TABLE `tblmatchresult`
  ADD CONSTRAINT `fk_matchresult_match` FOREIGN KEY (`match_id`) REFERENCES `tblmatch` (`id`),
  ADD CONSTRAINT `fk_matchresult_member` FOREIGN KEY (`member_id`) REFERENCES `tblmember` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
